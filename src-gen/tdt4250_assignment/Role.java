/**
 */
package tdt4250_assignment;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Role</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250_assignment.Role#getStaff <em>Staff</em>}</li>
 *   <li>{@link tdt4250_assignment.Role#getRoleType <em>Role Type</em>}</li>
 *   <li>{@link tdt4250_assignment.Role#getCourse <em>Course</em>}</li>
 * </ul>
 *
 * @see tdt4250_assignment.Tdt4250_assignmentPackage#getRole()
 * @model
 * @generated
 */
public interface Role extends EObject {
	/**
	 * Returns the value of the '<em><b>Staff</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link tdt4250_assignment.Person#getRoles <em>Roles</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Staff</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Staff</em>' reference.
	 * @see #setStaff(Person)
	 * @see tdt4250_assignment.Tdt4250_assignmentPackage#getRole_Staff()
	 * @see tdt4250_assignment.Person#getRoles
	 * @model opposite="roles" required="true"
	 * @generated
	 */
	Person getStaff();

	/**
	 * Sets the value of the '{@link tdt4250_assignment.Role#getStaff <em>Staff</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Staff</em>' reference.
	 * @see #getStaff()
	 * @generated
	 */
	void setStaff(Person value);

	/**
	 * Returns the value of the '<em><b>Role Type</b></em>' attribute.
	 * The default value is <code>"COORDINATOR"</code>.
	 * The literals are from the enumeration {@link tdt4250_assignment.RoleType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Role Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Role Type</em>' attribute.
	 * @see tdt4250_assignment.RoleType
	 * @see #setRoleType(RoleType)
	 * @see tdt4250_assignment.Tdt4250_assignmentPackage#getRole_RoleType()
	 * @model default="COORDINATOR" ordered="false"
	 * @generated
	 */
	RoleType getRoleType();

	/**
	 * Sets the value of the '{@link tdt4250_assignment.Role#getRoleType <em>Role Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Role Type</em>' attribute.
	 * @see tdt4250_assignment.RoleType
	 * @see #getRoleType()
	 * @generated
	 */
	void setRoleType(RoleType value);

	/**
	 * Returns the value of the '<em><b>Course</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link tdt4250_assignment.CourseInstance#getRoles <em>Roles</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Course</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Course</em>' container reference.
	 * @see #setCourse(CourseInstance)
	 * @see tdt4250_assignment.Tdt4250_assignmentPackage#getRole_Course()
	 * @see tdt4250_assignment.CourseInstance#getRoles
	 * @model opposite="roles" required="true" transient="false"
	 * @generated
	 */
	CourseInstance getCourse();

	/**
	 * Sets the value of the '{@link tdt4250_assignment.Role#getCourse <em>Course</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Course</em>' container reference.
	 * @see #getCourse()
	 * @generated
	 */
	void setCourse(CourseInstance value);

} // Role

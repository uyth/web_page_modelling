/**
 */
package tdt4250_assignment;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see tdt4250_assignment.Tdt4250_assignmentFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore validationDelegates='http://www.eclipse.org/acceleo/query/1.0 http://www.eclipse.org/emf/2002/Ecore/OCL'"
 * @generated
 */
public interface Tdt4250_assignmentPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "tdt4250_assignment";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "platform:/plugin/tdt4250_assignment";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "tdt4250_assignment";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Tdt4250_assignmentPackage eINSTANCE = tdt4250_assignment.impl.Tdt4250_assignmentPackageImpl.init();

	/**
	 * The meta object id for the '{@link tdt4250_assignment.impl.NamedImpl <em>Named</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250_assignment.impl.NamedImpl
	 * @see tdt4250_assignment.impl.Tdt4250_assignmentPackageImpl#getNamed()
	 * @generated
	 */
	int NAMED = 9;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED__NAME = 0;

	/**
	 * The number of structural features of the '<em>Named</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Named</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link tdt4250_assignment.impl.CourseImpl <em>Course</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250_assignment.impl.CourseImpl
	 * @see tdt4250_assignment.impl.Tdt4250_assignmentPackageImpl#getCourse()
	 * @generated
	 */
	int COURSE = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__NAME = NAMED__NAME;

	/**
	 * The feature id for the '<em><b>Course Instances</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__COURSE_INSTANCES = NAMED_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__CODE = NAMED_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Department</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__DEPARTMENT = NAMED_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Credit Reductions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__CREDIT_REDUCTIONS = NAMED_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Recommended</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__RECOMMENDED = NAMED_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Required</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__REQUIRED = NAMED_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Course</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_FEATURE_COUNT = NAMED_FEATURE_COUNT + 6;

	/**
	 * The number of operations of the '<em>Course</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_OPERATION_COUNT = NAMED_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link tdt4250_assignment.impl.CourseInstanceImpl <em>Course Instance</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250_assignment.impl.CourseInstanceImpl
	 * @see tdt4250_assignment.impl.Tdt4250_assignmentPackageImpl#getCourseInstance()
	 * @generated
	 */
	int COURSE_INSTANCE = 1;

	/**
	 * The feature id for the '<em><b>Evaluations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE__EVALUATIONS = 0;

	/**
	 * The feature id for the '<em><b>Time Table</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE__TIME_TABLE = 1;

	/**
	 * The feature id for the '<em><b>Roles</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE__ROLES = 2;

	/**
	 * The feature id for the '<em><b>Content</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE__CONTENT = 3;

	/**
	 * The feature id for the '<em><b>Credits</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE__CREDITS = 4;

	/**
	 * The feature id for the '<em><b>Study Programs</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE__STUDY_PROGRAMS = 5;

	/**
	 * The feature id for the '<em><b>Lecture Hours</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE__LECTURE_HOURS = 6;

	/**
	 * The feature id for the '<em><b>Lab Hours</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE__LAB_HOURS = 7;

	/**
	 * The feature id for the '<em><b>Year</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE__YEAR = 8;

	/**
	 * The feature id for the '<em><b>Semester</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE__SEMESTER = 9;

	/**
	 * The feature id for the '<em><b>Course</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE__COURSE = 10;

	/**
	 * The number of structural features of the '<em>Course Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE_FEATURE_COUNT = 11;

	/**
	 * The number of operations of the '<em>Course Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link tdt4250_assignment.impl.ReductionImpl <em>Reduction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250_assignment.impl.ReductionImpl
	 * @see tdt4250_assignment.impl.Tdt4250_assignmentPackageImpl#getReduction()
	 * @generated
	 */
	int REDUCTION = 2;

	/**
	 * The feature id for the '<em><b>Reduction</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REDUCTION__REDUCTION = 0;

	/**
	 * The feature id for the '<em><b>From</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REDUCTION__FROM = 1;

	/**
	 * The feature id for the '<em><b>To</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REDUCTION__TO = 2;

	/**
	 * The feature id for the '<em><b>Course</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REDUCTION__COURSE = 3;

	/**
	 * The feature id for the '<em><b>Reduction Course</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REDUCTION__REDUCTION_COURSE = 4;

	/**
	 * The number of structural features of the '<em>Reduction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REDUCTION_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Reduction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REDUCTION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link tdt4250_assignment.impl.RoleImpl <em>Role</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250_assignment.impl.RoleImpl
	 * @see tdt4250_assignment.impl.Tdt4250_assignmentPackageImpl#getRole()
	 * @generated
	 */
	int ROLE = 3;

	/**
	 * The feature id for the '<em><b>Staff</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE__STAFF = 0;

	/**
	 * The feature id for the '<em><b>Role Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE__ROLE_TYPE = 1;

	/**
	 * The feature id for the '<em><b>Course</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE__COURSE = 2;

	/**
	 * The number of structural features of the '<em>Role</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Role</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link tdt4250_assignment.impl.DepartmentImpl <em>Department</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250_assignment.impl.DepartmentImpl
	 * @see tdt4250_assignment.impl.Tdt4250_assignmentPackageImpl#getDepartment()
	 * @generated
	 */
	int DEPARTMENT = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPARTMENT__NAME = NAMED__NAME;

	/**
	 * The feature id for the '<em><b>Courses</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPARTMENT__COURSES = NAMED_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Staff</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPARTMENT__STAFF = NAMED_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Department</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPARTMENT_FEATURE_COUNT = NAMED_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Department</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPARTMENT_OPERATION_COUNT = NAMED_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link tdt4250_assignment.impl.PersonImpl <em>Person</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250_assignment.impl.PersonImpl
	 * @see tdt4250_assignment.impl.Tdt4250_assignmentPackageImpl#getPerson()
	 * @generated
	 */
	int PERSON = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON__NAME = NAMED__NAME;

	/**
	 * The feature id for the '<em><b>Department</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON__DEPARTMENT = NAMED_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Roles</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON__ROLES = NAMED_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Person</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON_FEATURE_COUNT = NAMED_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Person</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON_OPERATION_COUNT = NAMED_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link tdt4250_assignment.impl.EvaluationImpl <em>Evaluation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250_assignment.impl.EvaluationImpl
	 * @see tdt4250_assignment.impl.Tdt4250_assignmentPackageImpl#getEvaluation()
	 * @generated
	 */
	int EVALUATION = 6;

	/**
	 * The feature id for the '<em><b>Eval Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATION__EVAL_TYPE = 0;

	/**
	 * The feature id for the '<em><b>Percentage</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATION__PERCENTAGE = 1;

	/**
	 * The feature id for the '<em><b>Course Instance</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATION__COURSE_INSTANCE = 2;

	/**
	 * The feature id for the '<em><b>Required</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATION__REQUIRED = 3;

	/**
	 * The number of structural features of the '<em>Evaluation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATION_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Evaluation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link tdt4250_assignment.impl.TimeTableImpl <em>Time Table</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250_assignment.impl.TimeTableImpl
	 * @see tdt4250_assignment.impl.Tdt4250_assignmentPackageImpl#getTimeTable()
	 * @generated
	 */
	int TIME_TABLE = 7;

	/**
	 * The feature id for the '<em><b>Course</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_TABLE__COURSE = 0;

	/**
	 * The feature id for the '<em><b>Schedule</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_TABLE__SCHEDULE = 1;

	/**
	 * The number of structural features of the '<em>Time Table</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_TABLE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Time Table</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_TABLE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link tdt4250_assignment.impl.ScheduleSlotImpl <em>Schedule Slot</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250_assignment.impl.ScheduleSlotImpl
	 * @see tdt4250_assignment.impl.Tdt4250_assignmentPackageImpl#getScheduleSlot()
	 * @generated
	 */
	int SCHEDULE_SLOT = 8;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULE_SLOT__TYPE = 0;

	/**
	 * The feature id for the '<em><b>Time Table</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULE_SLOT__TIME_TABLE = 1;

	/**
	 * The feature id for the '<em><b>Day</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULE_SLOT__DAY = 2;

	/**
	 * The feature id for the '<em><b>From</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULE_SLOT__FROM = 3;

	/**
	 * The feature id for the '<em><b>To</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULE_SLOT__TO = 4;

	/**
	 * The feature id for the '<em><b>Room</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULE_SLOT__ROOM = 5;

	/**
	 * The feature id for the '<em><b>Study Programs</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULE_SLOT__STUDY_PROGRAMS = 6;

	/**
	 * The number of structural features of the '<em>Schedule Slot</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULE_SLOT_FEATURE_COUNT = 7;

	/**
	 * The number of operations of the '<em>Schedule Slot</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULE_SLOT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link tdt4250_assignment.SemesterType <em>Semester Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250_assignment.SemesterType
	 * @see tdt4250_assignment.impl.Tdt4250_assignmentPackageImpl#getSemesterType()
	 * @generated
	 */
	int SEMESTER_TYPE = 10;

	/**
	 * The meta object id for the '{@link tdt4250_assignment.CourseCode <em>Course Code</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250_assignment.CourseCode
	 * @see tdt4250_assignment.impl.Tdt4250_assignmentPackageImpl#getCourseCode()
	 * @generated
	 */
	int COURSE_CODE = 11;

	/**
	 * The meta object id for the '{@link tdt4250_assignment.StudyProgramCode <em>Study Program Code</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250_assignment.StudyProgramCode
	 * @see tdt4250_assignment.impl.Tdt4250_assignmentPackageImpl#getStudyProgramCode()
	 * @generated
	 */
	int STUDY_PROGRAM_CODE = 14;

	/**
	 * The meta object id for the '{@link tdt4250_assignment.Room <em>Room</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250_assignment.Room
	 * @see tdt4250_assignment.impl.Tdt4250_assignmentPackageImpl#getRoom()
	 * @generated
	 */
	int ROOM = 15;

	/**
	 * The meta object id for the '{@link tdt4250_assignment.ScheduleSlotType <em>Schedule Slot Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250_assignment.ScheduleSlotType
	 * @see tdt4250_assignment.impl.Tdt4250_assignmentPackageImpl#getScheduleSlotType()
	 * @generated
	 */
	int SCHEDULE_SLOT_TYPE = 16;

	/**
	 * The meta object id for the '{@link tdt4250_assignment.Day <em>Day</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250_assignment.Day
	 * @see tdt4250_assignment.impl.Tdt4250_assignmentPackageImpl#getDay()
	 * @generated
	 */
	int DAY = 17;

	/**
	 * The meta object id for the '{@link tdt4250_assignment.EvaluationType <em>Evaluation Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250_assignment.EvaluationType
	 * @see tdt4250_assignment.impl.Tdt4250_assignmentPackageImpl#getEvaluationType()
	 * @generated
	 */
	int EVALUATION_TYPE = 13;

	/**
	 * The meta object id for the '{@link tdt4250_assignment.RoleType <em>Role Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250_assignment.RoleType
	 * @see tdt4250_assignment.impl.Tdt4250_assignmentPackageImpl#getRoleType()
	 * @generated
	 */
	int ROLE_TYPE = 12;

	/**
	 * Returns the meta object for class '{@link tdt4250_assignment.Course <em>Course</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Course</em>'.
	 * @see tdt4250_assignment.Course
	 * @generated
	 */
	EClass getCourse();

	/**
	 * Returns the meta object for the containment reference list '{@link tdt4250_assignment.Course#getCourseInstances <em>Course Instances</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Course Instances</em>'.
	 * @see tdt4250_assignment.Course#getCourseInstances()
	 * @see #getCourse()
	 * @generated
	 */
	EReference getCourse_CourseInstances();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250_assignment.Course#getCode <em>Code</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Code</em>'.
	 * @see tdt4250_assignment.Course#getCode()
	 * @see #getCourse()
	 * @generated
	 */
	EAttribute getCourse_Code();

	/**
	 * Returns the meta object for the container reference '{@link tdt4250_assignment.Course#getDepartment <em>Department</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Department</em>'.
	 * @see tdt4250_assignment.Course#getDepartment()
	 * @see #getCourse()
	 * @generated
	 */
	EReference getCourse_Department();

	/**
	 * Returns the meta object for the reference list '{@link tdt4250_assignment.Course#getRecommended <em>Recommended</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Recommended</em>'.
	 * @see tdt4250_assignment.Course#getRecommended()
	 * @see #getCourse()
	 * @generated
	 */
	EReference getCourse_Recommended();

	/**
	 * Returns the meta object for the reference list '{@link tdt4250_assignment.Course#getRequired <em>Required</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Required</em>'.
	 * @see tdt4250_assignment.Course#getRequired()
	 * @see #getCourse()
	 * @generated
	 */
	EReference getCourse_Required();

	/**
	 * Returns the meta object for class '{@link tdt4250_assignment.CourseInstance <em>Course Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Course Instance</em>'.
	 * @see tdt4250_assignment.CourseInstance
	 * @generated
	 */
	EClass getCourseInstance();

	/**
	 * Returns the meta object for the containment reference list '{@link tdt4250_assignment.CourseInstance#getEvaluations <em>Evaluations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Evaluations</em>'.
	 * @see tdt4250_assignment.CourseInstance#getEvaluations()
	 * @see #getCourseInstance()
	 * @generated
	 */
	EReference getCourseInstance_Evaluations();

	/**
	 * Returns the meta object for the containment reference '{@link tdt4250_assignment.CourseInstance#getTimeTable <em>Time Table</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Time Table</em>'.
	 * @see tdt4250_assignment.CourseInstance#getTimeTable()
	 * @see #getCourseInstance()
	 * @generated
	 */
	EReference getCourseInstance_TimeTable();

	/**
	 * Returns the meta object for the containment reference list '{@link tdt4250_assignment.CourseInstance#getRoles <em>Roles</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Roles</em>'.
	 * @see tdt4250_assignment.CourseInstance#getRoles()
	 * @see #getCourseInstance()
	 * @generated
	 */
	EReference getCourseInstance_Roles();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250_assignment.CourseInstance#getContent <em>Content</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Content</em>'.
	 * @see tdt4250_assignment.CourseInstance#getContent()
	 * @see #getCourseInstance()
	 * @generated
	 */
	EAttribute getCourseInstance_Content();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250_assignment.CourseInstance#getCredits <em>Credits</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Credits</em>'.
	 * @see tdt4250_assignment.CourseInstance#getCredits()
	 * @see #getCourseInstance()
	 * @generated
	 */
	EAttribute getCourseInstance_Credits();

	/**
	 * Returns the meta object for the attribute list '{@link tdt4250_assignment.CourseInstance#getStudyPrograms <em>Study Programs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Study Programs</em>'.
	 * @see tdt4250_assignment.CourseInstance#getStudyPrograms()
	 * @see #getCourseInstance()
	 * @generated
	 */
	EAttribute getCourseInstance_StudyPrograms();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250_assignment.CourseInstance#getLectureHours <em>Lecture Hours</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lecture Hours</em>'.
	 * @see tdt4250_assignment.CourseInstance#getLectureHours()
	 * @see #getCourseInstance()
	 * @generated
	 */
	EAttribute getCourseInstance_LectureHours();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250_assignment.CourseInstance#getLabHours <em>Lab Hours</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lab Hours</em>'.
	 * @see tdt4250_assignment.CourseInstance#getLabHours()
	 * @see #getCourseInstance()
	 * @generated
	 */
	EAttribute getCourseInstance_LabHours();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250_assignment.CourseInstance#getYear <em>Year</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Year</em>'.
	 * @see tdt4250_assignment.CourseInstance#getYear()
	 * @see #getCourseInstance()
	 * @generated
	 */
	EAttribute getCourseInstance_Year();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250_assignment.CourseInstance#getSemester <em>Semester</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Semester</em>'.
	 * @see tdt4250_assignment.CourseInstance#getSemester()
	 * @see #getCourseInstance()
	 * @generated
	 */
	EAttribute getCourseInstance_Semester();

	/**
	 * Returns the meta object for the container reference '{@link tdt4250_assignment.CourseInstance#getCourse <em>Course</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Course</em>'.
	 * @see tdt4250_assignment.CourseInstance#getCourse()
	 * @see #getCourseInstance()
	 * @generated
	 */
	EReference getCourseInstance_Course();

	/**
	 * Returns the meta object for the containment reference list '{@link tdt4250_assignment.Course#getCreditReductions <em>Credit Reductions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Credit Reductions</em>'.
	 * @see tdt4250_assignment.Course#getCreditReductions()
	 * @see #getCourse()
	 * @generated
	 */
	EReference getCourse_CreditReductions();

	/**
	 * Returns the meta object for class '{@link tdt4250_assignment.Reduction <em>Reduction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Reduction</em>'.
	 * @see tdt4250_assignment.Reduction
	 * @generated
	 */
	EClass getReduction();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250_assignment.Reduction#getReduction <em>Reduction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Reduction</em>'.
	 * @see tdt4250_assignment.Reduction#getReduction()
	 * @see #getReduction()
	 * @generated
	 */
	EAttribute getReduction_Reduction();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250_assignment.Reduction#getFrom <em>From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>From</em>'.
	 * @see tdt4250_assignment.Reduction#getFrom()
	 * @see #getReduction()
	 * @generated
	 */
	EAttribute getReduction_From();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250_assignment.Reduction#getTo <em>To</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>To</em>'.
	 * @see tdt4250_assignment.Reduction#getTo()
	 * @see #getReduction()
	 * @generated
	 */
	EAttribute getReduction_To();

	/**
	 * Returns the meta object for the container reference '{@link tdt4250_assignment.Reduction#getCourse <em>Course</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Course</em>'.
	 * @see tdt4250_assignment.Reduction#getCourse()
	 * @see #getReduction()
	 * @generated
	 */
	EReference getReduction_Course();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250_assignment.Reduction#getReductionCourse <em>Reduction Course</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Reduction Course</em>'.
	 * @see tdt4250_assignment.Reduction#getReductionCourse()
	 * @see #getReduction()
	 * @generated
	 */
	EAttribute getReduction_ReductionCourse();

	/**
	 * Returns the meta object for class '{@link tdt4250_assignment.Role <em>Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Role</em>'.
	 * @see tdt4250_assignment.Role
	 * @generated
	 */
	EClass getRole();

	/**
	 * Returns the meta object for the reference '{@link tdt4250_assignment.Role#getStaff <em>Staff</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Staff</em>'.
	 * @see tdt4250_assignment.Role#getStaff()
	 * @see #getRole()
	 * @generated
	 */
	EReference getRole_Staff();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250_assignment.Role#getRoleType <em>Role Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Role Type</em>'.
	 * @see tdt4250_assignment.Role#getRoleType()
	 * @see #getRole()
	 * @generated
	 */
	EAttribute getRole_RoleType();

	/**
	 * Returns the meta object for the container reference '{@link tdt4250_assignment.Role#getCourse <em>Course</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Course</em>'.
	 * @see tdt4250_assignment.Role#getCourse()
	 * @see #getRole()
	 * @generated
	 */
	EReference getRole_Course();

	/**
	 * Returns the meta object for class '{@link tdt4250_assignment.Department <em>Department</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Department</em>'.
	 * @see tdt4250_assignment.Department
	 * @generated
	 */
	EClass getDepartment();

	/**
	 * Returns the meta object for the containment reference list '{@link tdt4250_assignment.Department#getCourses <em>Courses</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Courses</em>'.
	 * @see tdt4250_assignment.Department#getCourses()
	 * @see #getDepartment()
	 * @generated
	 */
	EReference getDepartment_Courses();

	/**
	 * Returns the meta object for the containment reference list '{@link tdt4250_assignment.Department#getStaff <em>Staff</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Staff</em>'.
	 * @see tdt4250_assignment.Department#getStaff()
	 * @see #getDepartment()
	 * @generated
	 */
	EReference getDepartment_Staff();

	/**
	 * Returns the meta object for class '{@link tdt4250_assignment.Person <em>Person</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Person</em>'.
	 * @see tdt4250_assignment.Person
	 * @generated
	 */
	EClass getPerson();

	/**
	 * Returns the meta object for the container reference '{@link tdt4250_assignment.Person#getDepartment <em>Department</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Department</em>'.
	 * @see tdt4250_assignment.Person#getDepartment()
	 * @see #getPerson()
	 * @generated
	 */
	EReference getPerson_Department();

	/**
	 * Returns the meta object for the reference list '{@link tdt4250_assignment.Person#getRoles <em>Roles</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Roles</em>'.
	 * @see tdt4250_assignment.Person#getRoles()
	 * @see #getPerson()
	 * @generated
	 */
	EReference getPerson_Roles();

	/**
	 * Returns the meta object for class '{@link tdt4250_assignment.Evaluation <em>Evaluation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Evaluation</em>'.
	 * @see tdt4250_assignment.Evaluation
	 * @generated
	 */
	EClass getEvaluation();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250_assignment.Evaluation#getEvalType <em>Eval Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Eval Type</em>'.
	 * @see tdt4250_assignment.Evaluation#getEvalType()
	 * @see #getEvaluation()
	 * @generated
	 */
	EAttribute getEvaluation_EvalType();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250_assignment.Evaluation#getPercentage <em>Percentage</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Percentage</em>'.
	 * @see tdt4250_assignment.Evaluation#getPercentage()
	 * @see #getEvaluation()
	 * @generated
	 */
	EAttribute getEvaluation_Percentage();

	/**
	 * Returns the meta object for the container reference '{@link tdt4250_assignment.Evaluation#getCourseInstance <em>Course Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Course Instance</em>'.
	 * @see tdt4250_assignment.Evaluation#getCourseInstance()
	 * @see #getEvaluation()
	 * @generated
	 */
	EReference getEvaluation_CourseInstance();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250_assignment.Evaluation#isRequired <em>Required</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Required</em>'.
	 * @see tdt4250_assignment.Evaluation#isRequired()
	 * @see #getEvaluation()
	 * @generated
	 */
	EAttribute getEvaluation_Required();

	/**
	 * Returns the meta object for class '{@link tdt4250_assignment.TimeTable <em>Time Table</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Time Table</em>'.
	 * @see tdt4250_assignment.TimeTable
	 * @generated
	 */
	EClass getTimeTable();

	/**
	 * Returns the meta object for the container reference '{@link tdt4250_assignment.TimeTable#getCourse <em>Course</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Course</em>'.
	 * @see tdt4250_assignment.TimeTable#getCourse()
	 * @see #getTimeTable()
	 * @generated
	 */
	EReference getTimeTable_Course();

	/**
	 * Returns the meta object for the containment reference list '{@link tdt4250_assignment.TimeTable#getSchedule <em>Schedule</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Schedule</em>'.
	 * @see tdt4250_assignment.TimeTable#getSchedule()
	 * @see #getTimeTable()
	 * @generated
	 */
	EReference getTimeTable_Schedule();

	/**
	 * Returns the meta object for class '{@link tdt4250_assignment.ScheduleSlot <em>Schedule Slot</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Schedule Slot</em>'.
	 * @see tdt4250_assignment.ScheduleSlot
	 * @generated
	 */
	EClass getScheduleSlot();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250_assignment.ScheduleSlot#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see tdt4250_assignment.ScheduleSlot#getType()
	 * @see #getScheduleSlot()
	 * @generated
	 */
	EAttribute getScheduleSlot_Type();

	/**
	 * Returns the meta object for the container reference '{@link tdt4250_assignment.ScheduleSlot#getTimeTable <em>Time Table</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Time Table</em>'.
	 * @see tdt4250_assignment.ScheduleSlot#getTimeTable()
	 * @see #getScheduleSlot()
	 * @generated
	 */
	EReference getScheduleSlot_TimeTable();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250_assignment.ScheduleSlot#getDay <em>Day</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Day</em>'.
	 * @see tdt4250_assignment.ScheduleSlot#getDay()
	 * @see #getScheduleSlot()
	 * @generated
	 */
	EAttribute getScheduleSlot_Day();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250_assignment.ScheduleSlot#getFrom <em>From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>From</em>'.
	 * @see tdt4250_assignment.ScheduleSlot#getFrom()
	 * @see #getScheduleSlot()
	 * @generated
	 */
	EAttribute getScheduleSlot_From();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250_assignment.ScheduleSlot#getTo <em>To</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>To</em>'.
	 * @see tdt4250_assignment.ScheduleSlot#getTo()
	 * @see #getScheduleSlot()
	 * @generated
	 */
	EAttribute getScheduleSlot_To();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250_assignment.ScheduleSlot#getRoom <em>Room</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Room</em>'.
	 * @see tdt4250_assignment.ScheduleSlot#getRoom()
	 * @see #getScheduleSlot()
	 * @generated
	 */
	EAttribute getScheduleSlot_Room();

	/**
	 * Returns the meta object for the attribute list '{@link tdt4250_assignment.ScheduleSlot#getStudyPrograms <em>Study Programs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Study Programs</em>'.
	 * @see tdt4250_assignment.ScheduleSlot#getStudyPrograms()
	 * @see #getScheduleSlot()
	 * @generated
	 */
	EAttribute getScheduleSlot_StudyPrograms();

	/**
	 * Returns the meta object for class '{@link tdt4250_assignment.Named <em>Named</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Named</em>'.
	 * @see tdt4250_assignment.Named
	 * @generated
	 */
	EClass getNamed();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250_assignment.Named#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see tdt4250_assignment.Named#getName()
	 * @see #getNamed()
	 * @generated
	 */
	EAttribute getNamed_Name();

	/**
	 * Returns the meta object for enum '{@link tdt4250_assignment.SemesterType <em>Semester Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Semester Type</em>'.
	 * @see tdt4250_assignment.SemesterType
	 * @generated
	 */
	EEnum getSemesterType();

	/**
	 * Returns the meta object for enum '{@link tdt4250_assignment.CourseCode <em>Course Code</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Course Code</em>'.
	 * @see tdt4250_assignment.CourseCode
	 * @generated
	 */
	EEnum getCourseCode();

	/**
	 * Returns the meta object for enum '{@link tdt4250_assignment.StudyProgramCode <em>Study Program Code</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Study Program Code</em>'.
	 * @see tdt4250_assignment.StudyProgramCode
	 * @generated
	 */
	EEnum getStudyProgramCode();

	/**
	 * Returns the meta object for enum '{@link tdt4250_assignment.Room <em>Room</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Room</em>'.
	 * @see tdt4250_assignment.Room
	 * @generated
	 */
	EEnum getRoom();

	/**
	 * Returns the meta object for enum '{@link tdt4250_assignment.ScheduleSlotType <em>Schedule Slot Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Schedule Slot Type</em>'.
	 * @see tdt4250_assignment.ScheduleSlotType
	 * @generated
	 */
	EEnum getScheduleSlotType();

	/**
	 * Returns the meta object for enum '{@link tdt4250_assignment.Day <em>Day</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Day</em>'.
	 * @see tdt4250_assignment.Day
	 * @generated
	 */
	EEnum getDay();

	/**
	 * Returns the meta object for enum '{@link tdt4250_assignment.EvaluationType <em>Evaluation Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Evaluation Type</em>'.
	 * @see tdt4250_assignment.EvaluationType
	 * @generated
	 */
	EEnum getEvaluationType();

	/**
	 * Returns the meta object for enum '{@link tdt4250_assignment.RoleType <em>Role Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Role Type</em>'.
	 * @see tdt4250_assignment.RoleType
	 * @generated
	 */
	EEnum getRoleType();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	Tdt4250_assignmentFactory getTdt4250_assignmentFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link tdt4250_assignment.impl.CourseImpl <em>Course</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250_assignment.impl.CourseImpl
		 * @see tdt4250_assignment.impl.Tdt4250_assignmentPackageImpl#getCourse()
		 * @generated
		 */
		EClass COURSE = eINSTANCE.getCourse();

		/**
		 * The meta object literal for the '<em><b>Course Instances</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE__COURSE_INSTANCES = eINSTANCE.getCourse_CourseInstances();

		/**
		 * The meta object literal for the '<em><b>Code</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE__CODE = eINSTANCE.getCourse_Code();

		/**
		 * The meta object literal for the '<em><b>Department</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE__DEPARTMENT = eINSTANCE.getCourse_Department();

		/**
		 * The meta object literal for the '<em><b>Recommended</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE__RECOMMENDED = eINSTANCE.getCourse_Recommended();

		/**
		 * The meta object literal for the '<em><b>Required</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE__REQUIRED = eINSTANCE.getCourse_Required();

		/**
		 * The meta object literal for the '{@link tdt4250_assignment.impl.CourseInstanceImpl <em>Course Instance</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250_assignment.impl.CourseInstanceImpl
		 * @see tdt4250_assignment.impl.Tdt4250_assignmentPackageImpl#getCourseInstance()
		 * @generated
		 */
		EClass COURSE_INSTANCE = eINSTANCE.getCourseInstance();

		/**
		 * The meta object literal for the '<em><b>Evaluations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE_INSTANCE__EVALUATIONS = eINSTANCE.getCourseInstance_Evaluations();

		/**
		 * The meta object literal for the '<em><b>Time Table</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE_INSTANCE__TIME_TABLE = eINSTANCE.getCourseInstance_TimeTable();

		/**
		 * The meta object literal for the '<em><b>Roles</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE_INSTANCE__ROLES = eINSTANCE.getCourseInstance_Roles();

		/**
		 * The meta object literal for the '<em><b>Content</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE_INSTANCE__CONTENT = eINSTANCE.getCourseInstance_Content();

		/**
		 * The meta object literal for the '<em><b>Credits</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE_INSTANCE__CREDITS = eINSTANCE.getCourseInstance_Credits();

		/**
		 * The meta object literal for the '<em><b>Study Programs</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE_INSTANCE__STUDY_PROGRAMS = eINSTANCE.getCourseInstance_StudyPrograms();

		/**
		 * The meta object literal for the '<em><b>Lecture Hours</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE_INSTANCE__LECTURE_HOURS = eINSTANCE.getCourseInstance_LectureHours();

		/**
		 * The meta object literal for the '<em><b>Lab Hours</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE_INSTANCE__LAB_HOURS = eINSTANCE.getCourseInstance_LabHours();

		/**
		 * The meta object literal for the '<em><b>Year</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE_INSTANCE__YEAR = eINSTANCE.getCourseInstance_Year();

		/**
		 * The meta object literal for the '<em><b>Semester</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE_INSTANCE__SEMESTER = eINSTANCE.getCourseInstance_Semester();

		/**
		 * The meta object literal for the '<em><b>Course</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE_INSTANCE__COURSE = eINSTANCE.getCourseInstance_Course();

		/**
		 * The meta object literal for the '<em><b>Credit Reductions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE__CREDIT_REDUCTIONS = eINSTANCE.getCourse_CreditReductions();

		/**
		 * The meta object literal for the '{@link tdt4250_assignment.impl.ReductionImpl <em>Reduction</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250_assignment.impl.ReductionImpl
		 * @see tdt4250_assignment.impl.Tdt4250_assignmentPackageImpl#getReduction()
		 * @generated
		 */
		EClass REDUCTION = eINSTANCE.getReduction();

		/**
		 * The meta object literal for the '<em><b>Reduction</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REDUCTION__REDUCTION = eINSTANCE.getReduction_Reduction();

		/**
		 * The meta object literal for the '<em><b>From</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REDUCTION__FROM = eINSTANCE.getReduction_From();

		/**
		 * The meta object literal for the '<em><b>To</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REDUCTION__TO = eINSTANCE.getReduction_To();

		/**
		 * The meta object literal for the '<em><b>Course</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REDUCTION__COURSE = eINSTANCE.getReduction_Course();

		/**
		 * The meta object literal for the '<em><b>Reduction Course</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REDUCTION__REDUCTION_COURSE = eINSTANCE.getReduction_ReductionCourse();

		/**
		 * The meta object literal for the '{@link tdt4250_assignment.impl.RoleImpl <em>Role</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250_assignment.impl.RoleImpl
		 * @see tdt4250_assignment.impl.Tdt4250_assignmentPackageImpl#getRole()
		 * @generated
		 */
		EClass ROLE = eINSTANCE.getRole();

		/**
		 * The meta object literal for the '<em><b>Staff</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROLE__STAFF = eINSTANCE.getRole_Staff();

		/**
		 * The meta object literal for the '<em><b>Role Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROLE__ROLE_TYPE = eINSTANCE.getRole_RoleType();

		/**
		 * The meta object literal for the '<em><b>Course</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROLE__COURSE = eINSTANCE.getRole_Course();

		/**
		 * The meta object literal for the '{@link tdt4250_assignment.impl.DepartmentImpl <em>Department</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250_assignment.impl.DepartmentImpl
		 * @see tdt4250_assignment.impl.Tdt4250_assignmentPackageImpl#getDepartment()
		 * @generated
		 */
		EClass DEPARTMENT = eINSTANCE.getDepartment();

		/**
		 * The meta object literal for the '<em><b>Courses</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPARTMENT__COURSES = eINSTANCE.getDepartment_Courses();

		/**
		 * The meta object literal for the '<em><b>Staff</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPARTMENT__STAFF = eINSTANCE.getDepartment_Staff();

		/**
		 * The meta object literal for the '{@link tdt4250_assignment.impl.PersonImpl <em>Person</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250_assignment.impl.PersonImpl
		 * @see tdt4250_assignment.impl.Tdt4250_assignmentPackageImpl#getPerson()
		 * @generated
		 */
		EClass PERSON = eINSTANCE.getPerson();

		/**
		 * The meta object literal for the '<em><b>Department</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSON__DEPARTMENT = eINSTANCE.getPerson_Department();

		/**
		 * The meta object literal for the '<em><b>Roles</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSON__ROLES = eINSTANCE.getPerson_Roles();

		/**
		 * The meta object literal for the '{@link tdt4250_assignment.impl.EvaluationImpl <em>Evaluation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250_assignment.impl.EvaluationImpl
		 * @see tdt4250_assignment.impl.Tdt4250_assignmentPackageImpl#getEvaluation()
		 * @generated
		 */
		EClass EVALUATION = eINSTANCE.getEvaluation();

		/**
		 * The meta object literal for the '<em><b>Eval Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVALUATION__EVAL_TYPE = eINSTANCE.getEvaluation_EvalType();

		/**
		 * The meta object literal for the '<em><b>Percentage</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVALUATION__PERCENTAGE = eINSTANCE.getEvaluation_Percentage();

		/**
		 * The meta object literal for the '<em><b>Course Instance</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVALUATION__COURSE_INSTANCE = eINSTANCE.getEvaluation_CourseInstance();

		/**
		 * The meta object literal for the '<em><b>Required</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVALUATION__REQUIRED = eINSTANCE.getEvaluation_Required();

		/**
		 * The meta object literal for the '{@link tdt4250_assignment.impl.TimeTableImpl <em>Time Table</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250_assignment.impl.TimeTableImpl
		 * @see tdt4250_assignment.impl.Tdt4250_assignmentPackageImpl#getTimeTable()
		 * @generated
		 */
		EClass TIME_TABLE = eINSTANCE.getTimeTable();

		/**
		 * The meta object literal for the '<em><b>Course</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TIME_TABLE__COURSE = eINSTANCE.getTimeTable_Course();

		/**
		 * The meta object literal for the '<em><b>Schedule</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TIME_TABLE__SCHEDULE = eINSTANCE.getTimeTable_Schedule();

		/**
		 * The meta object literal for the '{@link tdt4250_assignment.impl.ScheduleSlotImpl <em>Schedule Slot</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250_assignment.impl.ScheduleSlotImpl
		 * @see tdt4250_assignment.impl.Tdt4250_assignmentPackageImpl#getScheduleSlot()
		 * @generated
		 */
		EClass SCHEDULE_SLOT = eINSTANCE.getScheduleSlot();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCHEDULE_SLOT__TYPE = eINSTANCE.getScheduleSlot_Type();

		/**
		 * The meta object literal for the '<em><b>Time Table</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCHEDULE_SLOT__TIME_TABLE = eINSTANCE.getScheduleSlot_TimeTable();

		/**
		 * The meta object literal for the '<em><b>Day</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCHEDULE_SLOT__DAY = eINSTANCE.getScheduleSlot_Day();

		/**
		 * The meta object literal for the '<em><b>From</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCHEDULE_SLOT__FROM = eINSTANCE.getScheduleSlot_From();

		/**
		 * The meta object literal for the '<em><b>To</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCHEDULE_SLOT__TO = eINSTANCE.getScheduleSlot_To();

		/**
		 * The meta object literal for the '<em><b>Room</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCHEDULE_SLOT__ROOM = eINSTANCE.getScheduleSlot_Room();

		/**
		 * The meta object literal for the '<em><b>Study Programs</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCHEDULE_SLOT__STUDY_PROGRAMS = eINSTANCE.getScheduleSlot_StudyPrograms();

		/**
		 * The meta object literal for the '{@link tdt4250_assignment.impl.NamedImpl <em>Named</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250_assignment.impl.NamedImpl
		 * @see tdt4250_assignment.impl.Tdt4250_assignmentPackageImpl#getNamed()
		 * @generated
		 */
		EClass NAMED = eINSTANCE.getNamed();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NAMED__NAME = eINSTANCE.getNamed_Name();

		/**
		 * The meta object literal for the '{@link tdt4250_assignment.SemesterType <em>Semester Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250_assignment.SemesterType
		 * @see tdt4250_assignment.impl.Tdt4250_assignmentPackageImpl#getSemesterType()
		 * @generated
		 */
		EEnum SEMESTER_TYPE = eINSTANCE.getSemesterType();

		/**
		 * The meta object literal for the '{@link tdt4250_assignment.CourseCode <em>Course Code</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250_assignment.CourseCode
		 * @see tdt4250_assignment.impl.Tdt4250_assignmentPackageImpl#getCourseCode()
		 * @generated
		 */
		EEnum COURSE_CODE = eINSTANCE.getCourseCode();

		/**
		 * The meta object literal for the '{@link tdt4250_assignment.StudyProgramCode <em>Study Program Code</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250_assignment.StudyProgramCode
		 * @see tdt4250_assignment.impl.Tdt4250_assignmentPackageImpl#getStudyProgramCode()
		 * @generated
		 */
		EEnum STUDY_PROGRAM_CODE = eINSTANCE.getStudyProgramCode();

		/**
		 * The meta object literal for the '{@link tdt4250_assignment.Room <em>Room</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250_assignment.Room
		 * @see tdt4250_assignment.impl.Tdt4250_assignmentPackageImpl#getRoom()
		 * @generated
		 */
		EEnum ROOM = eINSTANCE.getRoom();

		/**
		 * The meta object literal for the '{@link tdt4250_assignment.ScheduleSlotType <em>Schedule Slot Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250_assignment.ScheduleSlotType
		 * @see tdt4250_assignment.impl.Tdt4250_assignmentPackageImpl#getScheduleSlotType()
		 * @generated
		 */
		EEnum SCHEDULE_SLOT_TYPE = eINSTANCE.getScheduleSlotType();

		/**
		 * The meta object literal for the '{@link tdt4250_assignment.Day <em>Day</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250_assignment.Day
		 * @see tdt4250_assignment.impl.Tdt4250_assignmentPackageImpl#getDay()
		 * @generated
		 */
		EEnum DAY = eINSTANCE.getDay();

		/**
		 * The meta object literal for the '{@link tdt4250_assignment.EvaluationType <em>Evaluation Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250_assignment.EvaluationType
		 * @see tdt4250_assignment.impl.Tdt4250_assignmentPackageImpl#getEvaluationType()
		 * @generated
		 */
		EEnum EVALUATION_TYPE = eINSTANCE.getEvaluationType();

		/**
		 * The meta object literal for the '{@link tdt4250_assignment.RoleType <em>Role Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250_assignment.RoleType
		 * @see tdt4250_assignment.impl.Tdt4250_assignmentPackageImpl#getRoleType()
		 * @generated
		 */
		EEnum ROLE_TYPE = eINSTANCE.getRoleType();

	}

} //Tdt4250_assignmentPackage

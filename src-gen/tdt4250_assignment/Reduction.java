/**
 */
package tdt4250_assignment;

import java.util.Date;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Reduction</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250_assignment.Reduction#getReduction <em>Reduction</em>}</li>
 *   <li>{@link tdt4250_assignment.Reduction#getFrom <em>From</em>}</li>
 *   <li>{@link tdt4250_assignment.Reduction#getTo <em>To</em>}</li>
 *   <li>{@link tdt4250_assignment.Reduction#getCourse <em>Course</em>}</li>
 *   <li>{@link tdt4250_assignment.Reduction#getReductionCourse <em>Reduction Course</em>}</li>
 * </ul>
 *
 * @see tdt4250_assignment.Tdt4250_assignmentPackage#getReduction()
 * @model
 * @generated
 */
public interface Reduction extends EObject {
	/**
	 * Returns the value of the '<em><b>Reduction</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reduction</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reduction</em>' attribute.
	 * @see #setReduction(double)
	 * @see tdt4250_assignment.Tdt4250_assignmentPackage#getReduction_Reduction()
	 * @model
	 * @generated
	 */
	double getReduction();

	/**
	 * Sets the value of the '{@link tdt4250_assignment.Reduction#getReduction <em>Reduction</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reduction</em>' attribute.
	 * @see #getReduction()
	 * @generated
	 */
	void setReduction(double value);

	/**
	 * Returns the value of the '<em><b>From</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>From</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>From</em>' attribute.
	 * @see #setFrom(Date)
	 * @see tdt4250_assignment.Tdt4250_assignmentPackage#getReduction_From()
	 * @model
	 * @generated
	 */
	Date getFrom();

	/**
	 * Sets the value of the '{@link tdt4250_assignment.Reduction#getFrom <em>From</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>From</em>' attribute.
	 * @see #getFrom()
	 * @generated
	 */
	void setFrom(Date value);

	/**
	 * Returns the value of the '<em><b>To</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>To</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>To</em>' attribute.
	 * @see #setTo(Date)
	 * @see tdt4250_assignment.Tdt4250_assignmentPackage#getReduction_To()
	 * @model
	 * @generated
	 */
	Date getTo();

	/**
	 * Sets the value of the '{@link tdt4250_assignment.Reduction#getTo <em>To</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>To</em>' attribute.
	 * @see #getTo()
	 * @generated
	 */
	void setTo(Date value);

	/**
	 * Returns the value of the '<em><b>Course</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link tdt4250_assignment.Course#getCreditReductions <em>Credit Reductions</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Course</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Course</em>' container reference.
	 * @see #setCourse(Course)
	 * @see tdt4250_assignment.Tdt4250_assignmentPackage#getReduction_Course()
	 * @see tdt4250_assignment.Course#getCreditReductions
	 * @model opposite="creditReductions" transient="false"
	 * @generated
	 */
	Course getCourse();

	/**
	 * Sets the value of the '{@link tdt4250_assignment.Reduction#getCourse <em>Course</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Course</em>' container reference.
	 * @see #getCourse()
	 * @generated
	 */
	void setCourse(Course value);

	/**
	 * Returns the value of the '<em><b>Reduction Course</b></em>' attribute.
	 * The literals are from the enumeration {@link tdt4250_assignment.CourseCode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reduction Course</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reduction Course</em>' attribute.
	 * @see tdt4250_assignment.CourseCode
	 * @see #setReductionCourse(CourseCode)
	 * @see tdt4250_assignment.Tdt4250_assignmentPackage#getReduction_ReductionCourse()
	 * @model
	 * @generated
	 */
	CourseCode getReductionCourse();

	/**
	 * Sets the value of the '{@link tdt4250_assignment.Reduction#getReductionCourse <em>Reduction Course</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reduction Course</em>' attribute.
	 * @see tdt4250_assignment.CourseCode
	 * @see #getReductionCourse()
	 * @generated
	 */
	void setReductionCourse(CourseCode value);

} // Reduction

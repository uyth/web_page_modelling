/**
 */
package tdt4250_assignment;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Course Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250_assignment.CourseInstance#getEvaluations <em>Evaluations</em>}</li>
 *   <li>{@link tdt4250_assignment.CourseInstance#getTimeTable <em>Time Table</em>}</li>
 *   <li>{@link tdt4250_assignment.CourseInstance#getRoles <em>Roles</em>}</li>
 *   <li>{@link tdt4250_assignment.CourseInstance#getContent <em>Content</em>}</li>
 *   <li>{@link tdt4250_assignment.CourseInstance#getCredits <em>Credits</em>}</li>
 *   <li>{@link tdt4250_assignment.CourseInstance#getStudyPrograms <em>Study Programs</em>}</li>
 *   <li>{@link tdt4250_assignment.CourseInstance#getLectureHours <em>Lecture Hours</em>}</li>
 *   <li>{@link tdt4250_assignment.CourseInstance#getLabHours <em>Lab Hours</em>}</li>
 *   <li>{@link tdt4250_assignment.CourseInstance#getYear <em>Year</em>}</li>
 *   <li>{@link tdt4250_assignment.CourseInstance#getSemester <em>Semester</em>}</li>
 *   <li>{@link tdt4250_assignment.CourseInstance#getCourse <em>Course</em>}</li>
 * </ul>
 *
 * @see tdt4250_assignment.Tdt4250_assignmentPackage#getCourseInstance()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='validateEvaluation validateRoles validateHours'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL validateEvaluation='self.evaluations.percentage -&gt; sum() = 100' validateRoles='self.roles -&gt; select(r | r.roleType = RoleType::COORDINATOR) -&gt; size() &gt;= 1' validateHours='null'"
 * @generated
 */
public interface CourseInstance extends EObject {
	/**
	 * Returns the value of the '<em><b>Evaluations</b></em>' containment reference list.
	 * The list contents are of type {@link tdt4250_assignment.Evaluation}.
	 * It is bidirectional and its opposite is '{@link tdt4250_assignment.Evaluation#getCourseInstance <em>Course Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Evaluations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Evaluations</em>' containment reference list.
	 * @see tdt4250_assignment.Tdt4250_assignmentPackage#getCourseInstance_Evaluations()
	 * @see tdt4250_assignment.Evaluation#getCourseInstance
	 * @model opposite="courseInstance" containment="true" required="true" ordered="false"
	 * @generated
	 */
	EList<Evaluation> getEvaluations();

	/**
	 * Returns the value of the '<em><b>Time Table</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link tdt4250_assignment.TimeTable#getCourse <em>Course</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Time Table</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Time Table</em>' containment reference.
	 * @see #setTimeTable(TimeTable)
	 * @see tdt4250_assignment.Tdt4250_assignmentPackage#getCourseInstance_TimeTable()
	 * @see tdt4250_assignment.TimeTable#getCourse
	 * @model opposite="course" containment="true" required="true"
	 * @generated
	 */
	TimeTable getTimeTable();

	/**
	 * Sets the value of the '{@link tdt4250_assignment.CourseInstance#getTimeTable <em>Time Table</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Time Table</em>' containment reference.
	 * @see #getTimeTable()
	 * @generated
	 */
	void setTimeTable(TimeTable value);

	/**
	 * Returns the value of the '<em><b>Roles</b></em>' containment reference list.
	 * The list contents are of type {@link tdt4250_assignment.Role}.
	 * It is bidirectional and its opposite is '{@link tdt4250_assignment.Role#getCourse <em>Course</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Roles</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Roles</em>' containment reference list.
	 * @see tdt4250_assignment.Tdt4250_assignmentPackage#getCourseInstance_Roles()
	 * @see tdt4250_assignment.Role#getCourse
	 * @model opposite="course" containment="true" required="true"
	 * @generated
	 */
	EList<Role> getRoles();

	/**
	 * Returns the value of the '<em><b>Content</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Content</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Content</em>' attribute.
	 * @see #setContent(String)
	 * @see tdt4250_assignment.Tdt4250_assignmentPackage#getCourseInstance_Content()
	 * @model
	 * @generated
	 */
	String getContent();

	/**
	 * Sets the value of the '{@link tdt4250_assignment.CourseInstance#getContent <em>Content</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Content</em>' attribute.
	 * @see #getContent()
	 * @generated
	 */
	void setContent(String value);

	/**
	 * Returns the value of the '<em><b>Credits</b></em>' attribute.
	 * The default value is <code>"7.5"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Credits</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Credits</em>' attribute.
	 * @see #setCredits(double)
	 * @see tdt4250_assignment.Tdt4250_assignmentPackage#getCourseInstance_Credits()
	 * @model default="7.5" required="true"
	 * @generated
	 */
	double getCredits();

	/**
	 * Sets the value of the '{@link tdt4250_assignment.CourseInstance#getCredits <em>Credits</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Credits</em>' attribute.
	 * @see #getCredits()
	 * @generated
	 */
	void setCredits(double value);

	/**
	 * Returns the value of the '<em><b>Study Programs</b></em>' attribute list.
	 * The list contents are of type {@link tdt4250_assignment.StudyProgramCode}.
	 * The literals are from the enumeration {@link tdt4250_assignment.StudyProgramCode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Study Programs</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Study Programs</em>' attribute list.
	 * @see tdt4250_assignment.StudyProgramCode
	 * @see tdt4250_assignment.Tdt4250_assignmentPackage#getCourseInstance_StudyPrograms()
	 * @model
	 * @generated
	 */
	EList<StudyProgramCode> getStudyPrograms();

	/**
	 * Returns the value of the '<em><b>Lecture Hours</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lecture Hours</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lecture Hours</em>' attribute.
	 * @see #setLectureHours(int)
	 * @see tdt4250_assignment.Tdt4250_assignmentPackage#getCourseInstance_LectureHours()
	 * @model
	 * @generated
	 */
	int getLectureHours();

	/**
	 * Sets the value of the '{@link tdt4250_assignment.CourseInstance#getLectureHours <em>Lecture Hours</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lecture Hours</em>' attribute.
	 * @see #getLectureHours()
	 * @generated
	 */
	void setLectureHours(int value);

	/**
	 * Returns the value of the '<em><b>Lab Hours</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lab Hours</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lab Hours</em>' attribute.
	 * @see #setLabHours(int)
	 * @see tdt4250_assignment.Tdt4250_assignmentPackage#getCourseInstance_LabHours()
	 * @model
	 * @generated
	 */
	int getLabHours();

	/**
	 * Sets the value of the '{@link tdt4250_assignment.CourseInstance#getLabHours <em>Lab Hours</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lab Hours</em>' attribute.
	 * @see #getLabHours()
	 * @generated
	 */
	void setLabHours(int value);

	/**
	 * Returns the value of the '<em><b>Year</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Year</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Year</em>' attribute.
	 * @see #setYear(int)
	 * @see tdt4250_assignment.Tdt4250_assignmentPackage#getCourseInstance_Year()
	 * @model required="true"
	 * @generated
	 */
	int getYear();

	/**
	 * Sets the value of the '{@link tdt4250_assignment.CourseInstance#getYear <em>Year</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Year</em>' attribute.
	 * @see #getYear()
	 * @generated
	 */
	void setYear(int value);

	/**
	 * Returns the value of the '<em><b>Semester</b></em>' attribute.
	 * The literals are from the enumeration {@link tdt4250_assignment.SemesterType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Semester</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Semester</em>' attribute.
	 * @see tdt4250_assignment.SemesterType
	 * @see #setSemester(SemesterType)
	 * @see tdt4250_assignment.Tdt4250_assignmentPackage#getCourseInstance_Semester()
	 * @model required="true"
	 * @generated
	 */
	SemesterType getSemester();

	/**
	 * Sets the value of the '{@link tdt4250_assignment.CourseInstance#getSemester <em>Semester</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Semester</em>' attribute.
	 * @see tdt4250_assignment.SemesterType
	 * @see #getSemester()
	 * @generated
	 */
	void setSemester(SemesterType value);

	/**
	 * Returns the value of the '<em><b>Course</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link tdt4250_assignment.Course#getCourseInstances <em>Course Instances</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Course</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Course</em>' container reference.
	 * @see #setCourse(Course)
	 * @see tdt4250_assignment.Tdt4250_assignmentPackage#getCourseInstance_Course()
	 * @see tdt4250_assignment.Course#getCourseInstances
	 * @model opposite="courseInstances" required="true" transient="false"
	 * @generated
	 */
	Course getCourse();

	/**
	 * Sets the value of the '{@link tdt4250_assignment.CourseInstance#getCourse <em>Course</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Course</em>' container reference.
	 * @see #getCourse()
	 * @generated
	 */
	void setCourse(Course value);

} // CourseInstance

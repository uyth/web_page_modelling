/**
 */
package tdt4250_assignment;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Department</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250_assignment.Department#getCourses <em>Courses</em>}</li>
 *   <li>{@link tdt4250_assignment.Department#getStaff <em>Staff</em>}</li>
 * </ul>
 *
 * @see tdt4250_assignment.Tdt4250_assignmentPackage#getDepartment()
 * @model
 * @generated
 */
public interface Department extends Named {
	/**
	 * Returns the value of the '<em><b>Courses</b></em>' containment reference list.
	 * The list contents are of type {@link tdt4250_assignment.Course}.
	 * It is bidirectional and its opposite is '{@link tdt4250_assignment.Course#getDepartment <em>Department</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Courses</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Courses</em>' containment reference list.
	 * @see tdt4250_assignment.Tdt4250_assignmentPackage#getDepartment_Courses()
	 * @see tdt4250_assignment.Course#getDepartment
	 * @model opposite="department" containment="true"
	 * @generated
	 */
	EList<Course> getCourses();

	/**
	 * Returns the value of the '<em><b>Staff</b></em>' containment reference list.
	 * The list contents are of type {@link tdt4250_assignment.Person}.
	 * It is bidirectional and its opposite is '{@link tdt4250_assignment.Person#getDepartment <em>Department</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Staff</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Staff</em>' containment reference list.
	 * @see tdt4250_assignment.Tdt4250_assignmentPackage#getDepartment_Staff()
	 * @see tdt4250_assignment.Person#getDepartment
	 * @model opposite="department" containment="true"
	 * @generated
	 */
	EList<Person> getStaff();

} // Department

/**
 */
package tdt4250_assignment;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Study Program</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250_assignment.StudyProgram#getCode <em>Code</em>}</li>
 * </ul>
 *
 * @see tdt4250_assignment.Tdt4250_assignmentPackage#getStudyProgram()
 * @model
 * @generated
 */
public interface StudyProgram extends Named {
	/**
	 * Returns the value of the '<em><b>Code</b></em>' attribute.
	 * The literals are from the enumeration {@link tdt4250_assignment.StudyProgramCode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Code</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Code</em>' attribute.
	 * @see tdt4250_assignment.StudyProgramCode
	 * @see #setCode(StudyProgramCode)
	 * @see tdt4250_assignment.Tdt4250_assignmentPackage#getStudyProgram_Code()
	 * @model
	 * @generated
	 */
	StudyProgramCode getCode();

	/**
	 * Sets the value of the '{@link tdt4250_assignment.StudyProgram#getCode <em>Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Code</em>' attribute.
	 * @see tdt4250_assignment.StudyProgramCode
	 * @see #getCode()
	 * @generated
	 */
	void setCode(StudyProgramCode value);

} // StudyProgram

/**
 */
package tdt4250_assignment;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Schedule Slot</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250_assignment.ScheduleSlot#getType <em>Type</em>}</li>
 *   <li>{@link tdt4250_assignment.ScheduleSlot#getTimeTable <em>Time Table</em>}</li>
 *   <li>{@link tdt4250_assignment.ScheduleSlot#getDay <em>Day</em>}</li>
 *   <li>{@link tdt4250_assignment.ScheduleSlot#getFrom <em>From</em>}</li>
 *   <li>{@link tdt4250_assignment.ScheduleSlot#getTo <em>To</em>}</li>
 *   <li>{@link tdt4250_assignment.ScheduleSlot#getRoom <em>Room</em>}</li>
 *   <li>{@link tdt4250_assignment.ScheduleSlot#getStudyPrograms <em>Study Programs</em>}</li>
 * </ul>
 *
 * @see tdt4250_assignment.Tdt4250_assignmentPackage#getScheduleSlot()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='validateScheduleSlot'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL validateScheduleSlot='self.to &gt; self.from'"
 * @generated
 */
public interface ScheduleSlot extends EObject {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The literals are from the enumeration {@link tdt4250_assignment.ScheduleSlotType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see tdt4250_assignment.ScheduleSlotType
	 * @see #setType(ScheduleSlotType)
	 * @see tdt4250_assignment.Tdt4250_assignmentPackage#getScheduleSlot_Type()
	 * @model
	 * @generated
	 */
	ScheduleSlotType getType();

	/**
	 * Sets the value of the '{@link tdt4250_assignment.ScheduleSlot#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see tdt4250_assignment.ScheduleSlotType
	 * @see #getType()
	 * @generated
	 */
	void setType(ScheduleSlotType value);

	/**
	 * Returns the value of the '<em><b>Time Table</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link tdt4250_assignment.TimeTable#getSchedule <em>Schedule</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Time Table</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Time Table</em>' container reference.
	 * @see #setTimeTable(TimeTable)
	 * @see tdt4250_assignment.Tdt4250_assignmentPackage#getScheduleSlot_TimeTable()
	 * @see tdt4250_assignment.TimeTable#getSchedule
	 * @model opposite="schedule" transient="false"
	 * @generated
	 */
	TimeTable getTimeTable();

	/**
	 * Sets the value of the '{@link tdt4250_assignment.ScheduleSlot#getTimeTable <em>Time Table</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Time Table</em>' container reference.
	 * @see #getTimeTable()
	 * @generated
	 */
	void setTimeTable(TimeTable value);

	/**
	 * Returns the value of the '<em><b>Day</b></em>' attribute.
	 * The literals are from the enumeration {@link tdt4250_assignment.Day}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Day</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Day</em>' attribute.
	 * @see tdt4250_assignment.Day
	 * @see #setDay(Day)
	 * @see tdt4250_assignment.Tdt4250_assignmentPackage#getScheduleSlot_Day()
	 * @model
	 * @generated
	 */
	Day getDay();

	/**
	 * Sets the value of the '{@link tdt4250_assignment.ScheduleSlot#getDay <em>Day</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Day</em>' attribute.
	 * @see tdt4250_assignment.Day
	 * @see #getDay()
	 * @generated
	 */
	void setDay(Day value);

	/**
	 * Returns the value of the '<em><b>From</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>From</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>From</em>' attribute.
	 * @see #setFrom(int)
	 * @see tdt4250_assignment.Tdt4250_assignmentPackage#getScheduleSlot_From()
	 * @model
	 * @generated
	 */
	int getFrom();

	/**
	 * Sets the value of the '{@link tdt4250_assignment.ScheduleSlot#getFrom <em>From</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>From</em>' attribute.
	 * @see #getFrom()
	 * @generated
	 */
	void setFrom(int value);

	/**
	 * Returns the value of the '<em><b>To</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>To</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>To</em>' attribute.
	 * @see #setTo(int)
	 * @see tdt4250_assignment.Tdt4250_assignmentPackage#getScheduleSlot_To()
	 * @model
	 * @generated
	 */
	int getTo();

	/**
	 * Sets the value of the '{@link tdt4250_assignment.ScheduleSlot#getTo <em>To</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>To</em>' attribute.
	 * @see #getTo()
	 * @generated
	 */
	void setTo(int value);

	/**
	 * Returns the value of the '<em><b>Room</b></em>' attribute.
	 * The literals are from the enumeration {@link tdt4250_assignment.Room}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room</em>' attribute.
	 * @see tdt4250_assignment.Room
	 * @see #setRoom(Room)
	 * @see tdt4250_assignment.Tdt4250_assignmentPackage#getScheduleSlot_Room()
	 * @model
	 * @generated
	 */
	Room getRoom();

	/**
	 * Sets the value of the '{@link tdt4250_assignment.ScheduleSlot#getRoom <em>Room</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Room</em>' attribute.
	 * @see tdt4250_assignment.Room
	 * @see #getRoom()
	 * @generated
	 */
	void setRoom(Room value);

	/**
	 * Returns the value of the '<em><b>Study Programs</b></em>' attribute list.
	 * The list contents are of type {@link tdt4250_assignment.StudyProgramCode}.
	 * The literals are from the enumeration {@link tdt4250_assignment.StudyProgramCode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Study Programs</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Study Programs</em>' attribute list.
	 * @see tdt4250_assignment.StudyProgramCode
	 * @see tdt4250_assignment.Tdt4250_assignmentPackage#getScheduleSlot_StudyPrograms()
	 * @model
	 * @generated
	 */
	EList<StudyProgramCode> getStudyPrograms();

} // ScheduleSlot

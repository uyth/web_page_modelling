/**
 */
package tdt4250_assignment;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Time Table</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250_assignment.TimeTable#getCourse <em>Course</em>}</li>
 *   <li>{@link tdt4250_assignment.TimeTable#getSchedule <em>Schedule</em>}</li>
 * </ul>
 *
 * @see tdt4250_assignment.Tdt4250_assignmentPackage#getTimeTable()
 * @model
 * @generated
 */
public interface TimeTable extends EObject {
	/**
	 * Returns the value of the '<em><b>Course</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link tdt4250_assignment.CourseInstance#getTimeTable <em>Time Table</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Course</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Course</em>' container reference.
	 * @see #setCourse(CourseInstance)
	 * @see tdt4250_assignment.Tdt4250_assignmentPackage#getTimeTable_Course()
	 * @see tdt4250_assignment.CourseInstance#getTimeTable
	 * @model opposite="timeTable" transient="false"
	 * @generated
	 */
	CourseInstance getCourse();

	/**
	 * Sets the value of the '{@link tdt4250_assignment.TimeTable#getCourse <em>Course</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Course</em>' container reference.
	 * @see #getCourse()
	 * @generated
	 */
	void setCourse(CourseInstance value);

	/**
	 * Returns the value of the '<em><b>Schedule</b></em>' containment reference list.
	 * The list contents are of type {@link tdt4250_assignment.ScheduleSlot}.
	 * It is bidirectional and its opposite is '{@link tdt4250_assignment.ScheduleSlot#getTimeTable <em>Time Table</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Schedule</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Schedule</em>' containment reference list.
	 * @see tdt4250_assignment.Tdt4250_assignmentPackage#getTimeTable_Schedule()
	 * @see tdt4250_assignment.ScheduleSlot#getTimeTable
	 * @model opposite="timeTable" containment="true"
	 * @generated
	 */
	EList<ScheduleSlot> getSchedule();

} // TimeTable

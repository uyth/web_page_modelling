/**
 */
package tdt4250_assignment;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Course</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250_assignment.Course#getCourseInstances <em>Course Instances</em>}</li>
 *   <li>{@link tdt4250_assignment.Course#getCode <em>Code</em>}</li>
 *   <li>{@link tdt4250_assignment.Course#getDepartment <em>Department</em>}</li>
 *   <li>{@link tdt4250_assignment.Course#getCreditReductions <em>Credit Reductions</em>}</li>
 *   <li>{@link tdt4250_assignment.Course#getRecommended <em>Recommended</em>}</li>
 *   <li>{@link tdt4250_assignment.Course#getRequired <em>Required</em>}</li>
 * </ul>
 *
 * @see tdt4250_assignment.Tdt4250_assignmentPackage#getCourse()
 * @model
 * @generated
 */
public interface Course extends Named {
	/**
	 * Returns the value of the '<em><b>Course Instances</b></em>' containment reference list.
	 * The list contents are of type {@link tdt4250_assignment.CourseInstance}.
	 * It is bidirectional and its opposite is '{@link tdt4250_assignment.CourseInstance#getCourse <em>Course</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Course Instances</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Course Instances</em>' containment reference list.
	 * @see tdt4250_assignment.Tdt4250_assignmentPackage#getCourse_CourseInstances()
	 * @see tdt4250_assignment.CourseInstance#getCourse
	 * @model opposite="course" containment="true" required="true"
	 * @generated
	 */
	EList<CourseInstance> getCourseInstances();

	/**
	 * Returns the value of the '<em><b>Code</b></em>' attribute.
	 * The literals are from the enumeration {@link tdt4250_assignment.CourseCode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Code</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Code</em>' attribute.
	 * @see tdt4250_assignment.CourseCode
	 * @see #setCode(CourseCode)
	 * @see tdt4250_assignment.Tdt4250_assignmentPackage#getCourse_Code()
	 * @model required="true"
	 * @generated
	 */
	CourseCode getCode();

	/**
	 * Sets the value of the '{@link tdt4250_assignment.Course#getCode <em>Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Code</em>' attribute.
	 * @see tdt4250_assignment.CourseCode
	 * @see #getCode()
	 * @generated
	 */
	void setCode(CourseCode value);

	/**
	 * Returns the value of the '<em><b>Department</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link tdt4250_assignment.Department#getCourses <em>Courses</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Department</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Department</em>' container reference.
	 * @see #setDepartment(Department)
	 * @see tdt4250_assignment.Tdt4250_assignmentPackage#getCourse_Department()
	 * @see tdt4250_assignment.Department#getCourses
	 * @model opposite="courses" resolveProxies="false" transient="false"
	 * @generated
	 */
	Department getDepartment();

	/**
	 * Sets the value of the '{@link tdt4250_assignment.Course#getDepartment <em>Department</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Department</em>' container reference.
	 * @see #getDepartment()
	 * @generated
	 */
	void setDepartment(Department value);

	/**
	 * Returns the value of the '<em><b>Recommended</b></em>' reference list.
	 * The list contents are of type {@link tdt4250_assignment.Course}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Recommended</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Recommended</em>' reference list.
	 * @see tdt4250_assignment.Tdt4250_assignmentPackage#getCourse_Recommended()
	 * @model
	 * @generated
	 */
	EList<Course> getRecommended();

	/**
	 * Returns the value of the '<em><b>Required</b></em>' reference list.
	 * The list contents are of type {@link tdt4250_assignment.Course}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Required</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Required</em>' reference list.
	 * @see tdt4250_assignment.Tdt4250_assignmentPackage#getCourse_Required()
	 * @model
	 * @generated
	 */
	EList<Course> getRequired();

	/**
	 * Returns the value of the '<em><b>Credit Reductions</b></em>' containment reference list.
	 * The list contents are of type {@link tdt4250_assignment.Reduction}.
	 * It is bidirectional and its opposite is '{@link tdt4250_assignment.Reduction#getCourse <em>Course</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Credit Reductions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Credit Reductions</em>' containment reference list.
	 * @see tdt4250_assignment.Tdt4250_assignmentPackage#getCourse_CreditReductions()
	 * @see tdt4250_assignment.Reduction#getCourse
	 * @model opposite="course" containment="true"
	 * @generated
	 */
	EList<Reduction> getCreditReductions();

} // Course

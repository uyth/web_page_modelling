/**
 */
package tdt4250_assignment;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Student</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see tdt4250_assignment.Tdt4250_assignmentPackage#getStudent()
 * @model
 * @generated
 */
public interface Student extends EObject {
} // Student

/**
 */
package tdt4250_assignment;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Evaluation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250_assignment.Evaluation#getEvalType <em>Eval Type</em>}</li>
 *   <li>{@link tdt4250_assignment.Evaluation#getPercentage <em>Percentage</em>}</li>
 *   <li>{@link tdt4250_assignment.Evaluation#getCourseInstance <em>Course Instance</em>}</li>
 *   <li>{@link tdt4250_assignment.Evaluation#isRequired <em>Required</em>}</li>
 * </ul>
 *
 * @see tdt4250_assignment.Tdt4250_assignmentPackage#getEvaluation()
 * @model
 * @generated
 */
public interface Evaluation extends EObject {
	/**
	 * Returns the value of the '<em><b>Eval Type</b></em>' attribute.
	 * The literals are from the enumeration {@link tdt4250_assignment.EvaluationType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Eval Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Eval Type</em>' attribute.
	 * @see tdt4250_assignment.EvaluationType
	 * @see #setEvalType(EvaluationType)
	 * @see tdt4250_assignment.Tdt4250_assignmentPackage#getEvaluation_EvalType()
	 * @model
	 * @generated
	 */
	EvaluationType getEvalType();

	/**
	 * Sets the value of the '{@link tdt4250_assignment.Evaluation#getEvalType <em>Eval Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Eval Type</em>' attribute.
	 * @see tdt4250_assignment.EvaluationType
	 * @see #getEvalType()
	 * @generated
	 */
	void setEvalType(EvaluationType value);

	/**
	 * Returns the value of the '<em><b>Percentage</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Percentage</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Percentage</em>' attribute.
	 * @see #setPercentage(int)
	 * @see tdt4250_assignment.Tdt4250_assignmentPackage#getEvaluation_Percentage()
	 * @model
	 * @generated
	 */
	int getPercentage();

	/**
	 * Sets the value of the '{@link tdt4250_assignment.Evaluation#getPercentage <em>Percentage</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Percentage</em>' attribute.
	 * @see #getPercentage()
	 * @generated
	 */
	void setPercentage(int value);

	/**
	 * Returns the value of the '<em><b>Course Instance</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link tdt4250_assignment.CourseInstance#getEvaluations <em>Evaluations</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Course Instance</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Course Instance</em>' container reference.
	 * @see tdt4250_assignment.Tdt4250_assignmentPackage#getEvaluation_CourseInstance()
	 * @see tdt4250_assignment.CourseInstance#getEvaluations
	 * @model opposite="evaluations" required="true" transient="false" changeable="false"
	 * @generated
	 */
	CourseInstance getCourseInstance();

	/**
	 * Returns the value of the '<em><b>Required</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Required</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Required</em>' attribute.
	 * @see #setRequired(boolean)
	 * @see tdt4250_assignment.Tdt4250_assignmentPackage#getEvaluation_Required()
	 * @model
	 * @generated
	 */
	boolean isRequired();

	/**
	 * Sets the value of the '{@link tdt4250_assignment.Evaluation#isRequired <em>Required</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Required</em>' attribute.
	 * @see #isRequired()
	 * @generated
	 */
	void setRequired(boolean value);

} // Evaluation

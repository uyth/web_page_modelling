/**
 */
package tdt4250_assignment;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Course Code</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see tdt4250_assignment.Tdt4250_assignmentPackage#getCourseCode()
 * @model
 * @generated
 */
public enum CourseCode implements Enumerator {
	/**
	 * The '<em><b>TDT4100</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TDT4100_VALUE
	 * @generated
	 * @ordered
	 */
	TDT4100(1, "TDT4100", "TDT4100"),

	/**
	 * The '<em><b>TDT4250</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TDT4250_VALUE
	 * @generated
	 * @ordered
	 */
	TDT4250(2, "TDT4250", "TDT4250"),

	/**
	 * The '<em><b>TDT4102</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TDT4102_VALUE
	 * @generated
	 * @ordered
	 */
	TDT4102(3, "TDT4102", "TDT4102");

	/**
	 * The '<em><b>TDT4100</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>TDT4100</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TDT4100
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int TDT4100_VALUE = 1;

	/**
	 * The '<em><b>TDT4250</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>TDT4250</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TDT4250
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int TDT4250_VALUE = 2;

	/**
	 * The '<em><b>TDT4102</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>TDT4102</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TDT4102
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int TDT4102_VALUE = 3;

	/**
	 * An array of all the '<em><b>Course Code</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final CourseCode[] VALUES_ARRAY = new CourseCode[] { TDT4100, TDT4250, TDT4102, };

	/**
	 * A public read-only list of all the '<em><b>Course Code</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<CourseCode> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Course Code</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static CourseCode get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			CourseCode result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Course Code</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static CourseCode getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			CourseCode result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Course Code</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static CourseCode get(int value) {
		switch (value) {
		case TDT4100_VALUE:
			return TDT4100;
		case TDT4250_VALUE:
			return TDT4250;
		case TDT4102_VALUE:
			return TDT4102;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private CourseCode(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
		return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}

} //CourseCode

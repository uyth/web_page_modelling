/**
 */
package tdt4250_assignment.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import tdt4250_assignment.CourseInstance;
import tdt4250_assignment.Evaluation;
import tdt4250_assignment.EvaluationType;
import tdt4250_assignment.Tdt4250_assignmentPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Evaluation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link tdt4250_assignment.impl.EvaluationImpl#getEvalType <em>Eval Type</em>}</li>
 *   <li>{@link tdt4250_assignment.impl.EvaluationImpl#getPercentage <em>Percentage</em>}</li>
 *   <li>{@link tdt4250_assignment.impl.EvaluationImpl#getCourseInstance <em>Course Instance</em>}</li>
 *   <li>{@link tdt4250_assignment.impl.EvaluationImpl#isRequired <em>Required</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EvaluationImpl extends MinimalEObjectImpl.Container implements Evaluation {
	/**
	 * The default value of the '{@link #getEvalType() <em>Eval Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEvalType()
	 * @generated
	 * @ordered
	 */
	protected static final EvaluationType EVAL_TYPE_EDEFAULT = EvaluationType.WORK;

	/**
	 * The cached value of the '{@link #getEvalType() <em>Eval Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEvalType()
	 * @generated
	 * @ordered
	 */
	protected EvaluationType evalType = EVAL_TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getPercentage() <em>Percentage</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPercentage()
	 * @generated
	 * @ordered
	 */
	protected static final int PERCENTAGE_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getPercentage() <em>Percentage</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPercentage()
	 * @generated
	 * @ordered
	 */
	protected int percentage = PERCENTAGE_EDEFAULT;

	/**
	 * The default value of the '{@link #isRequired() <em>Required</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isRequired()
	 * @generated
	 * @ordered
	 */
	protected static final boolean REQUIRED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isRequired() <em>Required</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isRequired()
	 * @generated
	 * @ordered
	 */
	protected boolean required = REQUIRED_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EvaluationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Tdt4250_assignmentPackage.Literals.EVALUATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EvaluationType getEvalType() {
		return evalType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEvalType(EvaluationType newEvalType) {
		EvaluationType oldEvalType = evalType;
		evalType = newEvalType == null ? EVAL_TYPE_EDEFAULT : newEvalType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Tdt4250_assignmentPackage.EVALUATION__EVAL_TYPE,
					oldEvalType, evalType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getPercentage() {
		return percentage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPercentage(int newPercentage) {
		int oldPercentage = percentage;
		percentage = newPercentage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Tdt4250_assignmentPackage.EVALUATION__PERCENTAGE,
					oldPercentage, percentage));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CourseInstance getCourseInstance() {
		if (eContainerFeatureID() != Tdt4250_assignmentPackage.EVALUATION__COURSE_INSTANCE)
			return null;
		return (CourseInstance) eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isRequired() {
		return required;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRequired(boolean newRequired) {
		boolean oldRequired = required;
		required = newRequired;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Tdt4250_assignmentPackage.EVALUATION__REQUIRED,
					oldRequired, required));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case Tdt4250_assignmentPackage.EVALUATION__COURSE_INSTANCE:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return eBasicSetContainer(otherEnd, Tdt4250_assignmentPackage.EVALUATION__COURSE_INSTANCE, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case Tdt4250_assignmentPackage.EVALUATION__COURSE_INSTANCE:
			return eBasicSetContainer(null, Tdt4250_assignmentPackage.EVALUATION__COURSE_INSTANCE, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
		case Tdt4250_assignmentPackage.EVALUATION__COURSE_INSTANCE:
			return eInternalContainer().eInverseRemove(this, Tdt4250_assignmentPackage.COURSE_INSTANCE__EVALUATIONS,
					CourseInstance.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Tdt4250_assignmentPackage.EVALUATION__EVAL_TYPE:
			return getEvalType();
		case Tdt4250_assignmentPackage.EVALUATION__PERCENTAGE:
			return getPercentage();
		case Tdt4250_assignmentPackage.EVALUATION__COURSE_INSTANCE:
			return getCourseInstance();
		case Tdt4250_assignmentPackage.EVALUATION__REQUIRED:
			return isRequired();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Tdt4250_assignmentPackage.EVALUATION__EVAL_TYPE:
			setEvalType((EvaluationType) newValue);
			return;
		case Tdt4250_assignmentPackage.EVALUATION__PERCENTAGE:
			setPercentage((Integer) newValue);
			return;
		case Tdt4250_assignmentPackage.EVALUATION__REQUIRED:
			setRequired((Boolean) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Tdt4250_assignmentPackage.EVALUATION__EVAL_TYPE:
			setEvalType(EVAL_TYPE_EDEFAULT);
			return;
		case Tdt4250_assignmentPackage.EVALUATION__PERCENTAGE:
			setPercentage(PERCENTAGE_EDEFAULT);
			return;
		case Tdt4250_assignmentPackage.EVALUATION__REQUIRED:
			setRequired(REQUIRED_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Tdt4250_assignmentPackage.EVALUATION__EVAL_TYPE:
			return evalType != EVAL_TYPE_EDEFAULT;
		case Tdt4250_assignmentPackage.EVALUATION__PERCENTAGE:
			return percentage != PERCENTAGE_EDEFAULT;
		case Tdt4250_assignmentPackage.EVALUATION__COURSE_INSTANCE:
			return getCourseInstance() != null;
		case Tdt4250_assignmentPackage.EVALUATION__REQUIRED:
			return required != REQUIRED_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (evalType: ");
		result.append(evalType);
		result.append(", percentage: ");
		result.append(percentage);
		result.append(", required: ");
		result.append(required);
		result.append(')');
		return result.toString();
	}

} //EvaluationImpl

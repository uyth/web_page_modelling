/**
 */
package tdt4250_assignment.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;
import org.eclipse.emf.ecore.util.EcoreUtil;

import tdt4250_assignment.Day;
import tdt4250_assignment.Room;
import tdt4250_assignment.ScheduleSlot;
import tdt4250_assignment.ScheduleSlotType;
import tdt4250_assignment.StudyProgramCode;
import tdt4250_assignment.Tdt4250_assignmentPackage;
import tdt4250_assignment.TimeTable;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Schedule Slot</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link tdt4250_assignment.impl.ScheduleSlotImpl#getType <em>Type</em>}</li>
 *   <li>{@link tdt4250_assignment.impl.ScheduleSlotImpl#getTimeTable <em>Time Table</em>}</li>
 *   <li>{@link tdt4250_assignment.impl.ScheduleSlotImpl#getDay <em>Day</em>}</li>
 *   <li>{@link tdt4250_assignment.impl.ScheduleSlotImpl#getFrom <em>From</em>}</li>
 *   <li>{@link tdt4250_assignment.impl.ScheduleSlotImpl#getTo <em>To</em>}</li>
 *   <li>{@link tdt4250_assignment.impl.ScheduleSlotImpl#getRoom <em>Room</em>}</li>
 *   <li>{@link tdt4250_assignment.impl.ScheduleSlotImpl#getStudyPrograms <em>Study Programs</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ScheduleSlotImpl extends MinimalEObjectImpl.Container implements ScheduleSlot {
	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final ScheduleSlotType TYPE_EDEFAULT = ScheduleSlotType.LECTURE;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected ScheduleSlotType type = TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getDay() <em>Day</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDay()
	 * @generated
	 * @ordered
	 */
	protected static final Day DAY_EDEFAULT = Day.MONDAY;

	/**
	 * The cached value of the '{@link #getDay() <em>Day</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDay()
	 * @generated
	 * @ordered
	 */
	protected Day day = DAY_EDEFAULT;

	/**
	 * The default value of the '{@link #getFrom() <em>From</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFrom()
	 * @generated
	 * @ordered
	 */
	protected static final int FROM_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getFrom() <em>From</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFrom()
	 * @generated
	 * @ordered
	 */
	protected int from = FROM_EDEFAULT;

	/**
	 * The default value of the '{@link #getTo() <em>To</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTo()
	 * @generated
	 * @ordered
	 */
	protected static final int TO_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getTo() <em>To</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTo()
	 * @generated
	 * @ordered
	 */
	protected int to = TO_EDEFAULT;

	/**
	 * The default value of the '{@link #getRoom() <em>Room</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoom()
	 * @generated
	 * @ordered
	 */
	protected static final Room ROOM_EDEFAULT = Room.K5;

	/**
	 * The cached value of the '{@link #getRoom() <em>Room</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoom()
	 * @generated
	 * @ordered
	 */
	protected Room room = ROOM_EDEFAULT;

	/**
	 * The cached value of the '{@link #getStudyPrograms() <em>Study Programs</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStudyPrograms()
	 * @generated
	 * @ordered
	 */
	protected EList<StudyProgramCode> studyPrograms;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ScheduleSlotImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Tdt4250_assignmentPackage.Literals.SCHEDULE_SLOT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScheduleSlotType getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(ScheduleSlotType newType) {
		ScheduleSlotType oldType = type;
		type = newType == null ? TYPE_EDEFAULT : newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Tdt4250_assignmentPackage.SCHEDULE_SLOT__TYPE,
					oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimeTable getTimeTable() {
		if (eContainerFeatureID() != Tdt4250_assignmentPackage.SCHEDULE_SLOT__TIME_TABLE)
			return null;
		return (TimeTable) eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTimeTable(TimeTable newTimeTable, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject) newTimeTable, Tdt4250_assignmentPackage.SCHEDULE_SLOT__TIME_TABLE,
				msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTimeTable(TimeTable newTimeTable) {
		if (newTimeTable != eInternalContainer()
				|| (eContainerFeatureID() != Tdt4250_assignmentPackage.SCHEDULE_SLOT__TIME_TABLE
						&& newTimeTable != null)) {
			if (EcoreUtil.isAncestor(this, newTimeTable))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newTimeTable != null)
				msgs = ((InternalEObject) newTimeTable).eInverseAdd(this,
						Tdt4250_assignmentPackage.TIME_TABLE__SCHEDULE, TimeTable.class, msgs);
			msgs = basicSetTimeTable(newTimeTable, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Tdt4250_assignmentPackage.SCHEDULE_SLOT__TIME_TABLE,
					newTimeTable, newTimeTable));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Day getDay() {
		return day;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDay(Day newDay) {
		Day oldDay = day;
		day = newDay == null ? DAY_EDEFAULT : newDay;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Tdt4250_assignmentPackage.SCHEDULE_SLOT__DAY, oldDay,
					day));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getFrom() {
		return from;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFrom(int newFrom) {
		int oldFrom = from;
		from = newFrom;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Tdt4250_assignmentPackage.SCHEDULE_SLOT__FROM,
					oldFrom, from));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getTo() {
		return to;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTo(int newTo) {
		int oldTo = to;
		to = newTo;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Tdt4250_assignmentPackage.SCHEDULE_SLOT__TO, oldTo,
					to));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Room getRoom() {
		return room;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRoom(Room newRoom) {
		Room oldRoom = room;
		room = newRoom == null ? ROOM_EDEFAULT : newRoom;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Tdt4250_assignmentPackage.SCHEDULE_SLOT__ROOM,
					oldRoom, room));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<StudyProgramCode> getStudyPrograms() {
		if (studyPrograms == null) {
			studyPrograms = new EDataTypeUniqueEList<StudyProgramCode>(StudyProgramCode.class, this,
					Tdt4250_assignmentPackage.SCHEDULE_SLOT__STUDY_PROGRAMS);
		}
		return studyPrograms;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case Tdt4250_assignmentPackage.SCHEDULE_SLOT__TIME_TABLE:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetTimeTable((TimeTable) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case Tdt4250_assignmentPackage.SCHEDULE_SLOT__TIME_TABLE:
			return basicSetTimeTable(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
		case Tdt4250_assignmentPackage.SCHEDULE_SLOT__TIME_TABLE:
			return eInternalContainer().eInverseRemove(this, Tdt4250_assignmentPackage.TIME_TABLE__SCHEDULE,
					TimeTable.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Tdt4250_assignmentPackage.SCHEDULE_SLOT__TYPE:
			return getType();
		case Tdt4250_assignmentPackage.SCHEDULE_SLOT__TIME_TABLE:
			return getTimeTable();
		case Tdt4250_assignmentPackage.SCHEDULE_SLOT__DAY:
			return getDay();
		case Tdt4250_assignmentPackage.SCHEDULE_SLOT__FROM:
			return getFrom();
		case Tdt4250_assignmentPackage.SCHEDULE_SLOT__TO:
			return getTo();
		case Tdt4250_assignmentPackage.SCHEDULE_SLOT__ROOM:
			return getRoom();
		case Tdt4250_assignmentPackage.SCHEDULE_SLOT__STUDY_PROGRAMS:
			return getStudyPrograms();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Tdt4250_assignmentPackage.SCHEDULE_SLOT__TYPE:
			setType((ScheduleSlotType) newValue);
			return;
		case Tdt4250_assignmentPackage.SCHEDULE_SLOT__TIME_TABLE:
			setTimeTable((TimeTable) newValue);
			return;
		case Tdt4250_assignmentPackage.SCHEDULE_SLOT__DAY:
			setDay((Day) newValue);
			return;
		case Tdt4250_assignmentPackage.SCHEDULE_SLOT__FROM:
			setFrom((Integer) newValue);
			return;
		case Tdt4250_assignmentPackage.SCHEDULE_SLOT__TO:
			setTo((Integer) newValue);
			return;
		case Tdt4250_assignmentPackage.SCHEDULE_SLOT__ROOM:
			setRoom((Room) newValue);
			return;
		case Tdt4250_assignmentPackage.SCHEDULE_SLOT__STUDY_PROGRAMS:
			getStudyPrograms().clear();
			getStudyPrograms().addAll((Collection<? extends StudyProgramCode>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Tdt4250_assignmentPackage.SCHEDULE_SLOT__TYPE:
			setType(TYPE_EDEFAULT);
			return;
		case Tdt4250_assignmentPackage.SCHEDULE_SLOT__TIME_TABLE:
			setTimeTable((TimeTable) null);
			return;
		case Tdt4250_assignmentPackage.SCHEDULE_SLOT__DAY:
			setDay(DAY_EDEFAULT);
			return;
		case Tdt4250_assignmentPackage.SCHEDULE_SLOT__FROM:
			setFrom(FROM_EDEFAULT);
			return;
		case Tdt4250_assignmentPackage.SCHEDULE_SLOT__TO:
			setTo(TO_EDEFAULT);
			return;
		case Tdt4250_assignmentPackage.SCHEDULE_SLOT__ROOM:
			setRoom(ROOM_EDEFAULT);
			return;
		case Tdt4250_assignmentPackage.SCHEDULE_SLOT__STUDY_PROGRAMS:
			getStudyPrograms().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Tdt4250_assignmentPackage.SCHEDULE_SLOT__TYPE:
			return type != TYPE_EDEFAULT;
		case Tdt4250_assignmentPackage.SCHEDULE_SLOT__TIME_TABLE:
			return getTimeTable() != null;
		case Tdt4250_assignmentPackage.SCHEDULE_SLOT__DAY:
			return day != DAY_EDEFAULT;
		case Tdt4250_assignmentPackage.SCHEDULE_SLOT__FROM:
			return from != FROM_EDEFAULT;
		case Tdt4250_assignmentPackage.SCHEDULE_SLOT__TO:
			return to != TO_EDEFAULT;
		case Tdt4250_assignmentPackage.SCHEDULE_SLOT__ROOM:
			return room != ROOM_EDEFAULT;
		case Tdt4250_assignmentPackage.SCHEDULE_SLOT__STUDY_PROGRAMS:
			return studyPrograms != null && !studyPrograms.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (type: ");
		result.append(type);
		result.append(", day: ");
		result.append(day);
		result.append(", from: ");
		result.append(from);
		result.append(", to: ");
		result.append(to);
		result.append(", room: ");
		result.append(room);
		result.append(", studyPrograms: ");
		result.append(studyPrograms);
		result.append(')');
		return result.toString();
	}

} //ScheduleSlotImpl

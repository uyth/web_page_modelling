/**
 */
package tdt4250_assignment.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;
import tdt4250_assignment.CourseInstance;
import tdt4250_assignment.Person;
import tdt4250_assignment.Role;
import tdt4250_assignment.RoleType;
import tdt4250_assignment.Tdt4250_assignmentPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Role</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link tdt4250_assignment.impl.RoleImpl#getStaff <em>Staff</em>}</li>
 *   <li>{@link tdt4250_assignment.impl.RoleImpl#getRoleType <em>Role Type</em>}</li>
 *   <li>{@link tdt4250_assignment.impl.RoleImpl#getCourse <em>Course</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RoleImpl extends MinimalEObjectImpl.Container implements Role {
	/**
	 * The cached value of the '{@link #getStaff() <em>Staff</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStaff()
	 * @generated
	 * @ordered
	 */
	protected Person staff;

	/**
	 * The default value of the '{@link #getRoleType() <em>Role Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoleType()
	 * @generated
	 * @ordered
	 */
	protected static final RoleType ROLE_TYPE_EDEFAULT = RoleType.COORDINATOR;

	/**
	 * The cached value of the '{@link #getRoleType() <em>Role Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoleType()
	 * @generated
	 * @ordered
	 */
	protected RoleType roleType = ROLE_TYPE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RoleImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Tdt4250_assignmentPackage.Literals.ROLE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Person getStaff() {
		if (staff != null && staff.eIsProxy()) {
			InternalEObject oldStaff = (InternalEObject) staff;
			staff = (Person) eResolveProxy(oldStaff);
			if (staff != oldStaff) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Tdt4250_assignmentPackage.ROLE__STAFF,
							oldStaff, staff));
			}
		}
		return staff;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Person basicGetStaff() {
		return staff;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetStaff(Person newStaff, NotificationChain msgs) {
		Person oldStaff = staff;
		staff = newStaff;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					Tdt4250_assignmentPackage.ROLE__STAFF, oldStaff, newStaff);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStaff(Person newStaff) {
		if (newStaff != staff) {
			NotificationChain msgs = null;
			if (staff != null)
				msgs = ((InternalEObject) staff).eInverseRemove(this, Tdt4250_assignmentPackage.PERSON__ROLES,
						Person.class, msgs);
			if (newStaff != null)
				msgs = ((InternalEObject) newStaff).eInverseAdd(this, Tdt4250_assignmentPackage.PERSON__ROLES,
						Person.class, msgs);
			msgs = basicSetStaff(newStaff, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Tdt4250_assignmentPackage.ROLE__STAFF, newStaff,
					newStaff));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoleType getRoleType() {
		return roleType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRoleType(RoleType newRoleType) {
		RoleType oldRoleType = roleType;
		roleType = newRoleType == null ? ROLE_TYPE_EDEFAULT : newRoleType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Tdt4250_assignmentPackage.ROLE__ROLE_TYPE,
					oldRoleType, roleType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CourseInstance getCourse() {
		if (eContainerFeatureID() != Tdt4250_assignmentPackage.ROLE__COURSE)
			return null;
		return (CourseInstance) eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCourse(CourseInstance newCourse, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject) newCourse, Tdt4250_assignmentPackage.ROLE__COURSE, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCourse(CourseInstance newCourse) {
		if (newCourse != eInternalContainer()
				|| (eContainerFeatureID() != Tdt4250_assignmentPackage.ROLE__COURSE && newCourse != null)) {
			if (EcoreUtil.isAncestor(this, newCourse))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newCourse != null)
				msgs = ((InternalEObject) newCourse).eInverseAdd(this, Tdt4250_assignmentPackage.COURSE_INSTANCE__ROLES,
						CourseInstance.class, msgs);
			msgs = basicSetCourse(newCourse, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Tdt4250_assignmentPackage.ROLE__COURSE, newCourse,
					newCourse));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case Tdt4250_assignmentPackage.ROLE__STAFF:
			if (staff != null)
				msgs = ((InternalEObject) staff).eInverseRemove(this, Tdt4250_assignmentPackage.PERSON__ROLES,
						Person.class, msgs);
			return basicSetStaff((Person) otherEnd, msgs);
		case Tdt4250_assignmentPackage.ROLE__COURSE:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetCourse((CourseInstance) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case Tdt4250_assignmentPackage.ROLE__STAFF:
			return basicSetStaff(null, msgs);
		case Tdt4250_assignmentPackage.ROLE__COURSE:
			return basicSetCourse(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
		case Tdt4250_assignmentPackage.ROLE__COURSE:
			return eInternalContainer().eInverseRemove(this, Tdt4250_assignmentPackage.COURSE_INSTANCE__ROLES,
					CourseInstance.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Tdt4250_assignmentPackage.ROLE__STAFF:
			if (resolve)
				return getStaff();
			return basicGetStaff();
		case Tdt4250_assignmentPackage.ROLE__ROLE_TYPE:
			return getRoleType();
		case Tdt4250_assignmentPackage.ROLE__COURSE:
			return getCourse();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Tdt4250_assignmentPackage.ROLE__STAFF:
			setStaff((Person) newValue);
			return;
		case Tdt4250_assignmentPackage.ROLE__ROLE_TYPE:
			setRoleType((RoleType) newValue);
			return;
		case Tdt4250_assignmentPackage.ROLE__COURSE:
			setCourse((CourseInstance) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Tdt4250_assignmentPackage.ROLE__STAFF:
			setStaff((Person) null);
			return;
		case Tdt4250_assignmentPackage.ROLE__ROLE_TYPE:
			setRoleType(ROLE_TYPE_EDEFAULT);
			return;
		case Tdt4250_assignmentPackage.ROLE__COURSE:
			setCourse((CourseInstance) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Tdt4250_assignmentPackage.ROLE__STAFF:
			return staff != null;
		case Tdt4250_assignmentPackage.ROLE__ROLE_TYPE:
			return roleType != ROLE_TYPE_EDEFAULT;
		case Tdt4250_assignmentPackage.ROLE__COURSE:
			return getCourse() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (roleType: ");
		result.append(roleType);
		result.append(')');
		return result.toString();
	}

} //RoleImpl

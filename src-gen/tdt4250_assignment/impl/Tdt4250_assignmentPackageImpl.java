/**
 */
package tdt4250_assignment.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EValidator;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import tdt4250_assignment.Course;
import tdt4250_assignment.CourseCode;
import tdt4250_assignment.CourseInstance;
import tdt4250_assignment.Day;
import tdt4250_assignment.Department;
import tdt4250_assignment.Evaluation;
import tdt4250_assignment.EvaluationType;
import tdt4250_assignment.Named;
import tdt4250_assignment.Person;
import tdt4250_assignment.Reduction;
import tdt4250_assignment.Role;
import tdt4250_assignment.RoleType;
import tdt4250_assignment.Room;
import tdt4250_assignment.ScheduleSlot;
import tdt4250_assignment.ScheduleSlotType;
import tdt4250_assignment.SemesterType;
import tdt4250_assignment.StudyProgramCode;
import tdt4250_assignment.Tdt4250_assignmentFactory;
import tdt4250_assignment.Tdt4250_assignmentPackage;
import tdt4250_assignment.TimeTable;

import tdt4250_assignment.util.Tdt4250_assignmentValidator;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Tdt4250_assignmentPackageImpl extends EPackageImpl implements Tdt4250_assignmentPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass courseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass courseInstanceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass reductionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass roleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass departmentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass personEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass evaluationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass timeTableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scheduleSlotEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass namedEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum semesterTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum courseCodeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum studyProgramCodeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum roomEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum scheduleSlotTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum dayEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum evaluationTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum roleTypeEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see tdt4250_assignment.Tdt4250_assignmentPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private Tdt4250_assignmentPackageImpl() {
		super(eNS_URI, Tdt4250_assignmentFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link Tdt4250_assignmentPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static Tdt4250_assignmentPackage init() {
		if (isInited)
			return (Tdt4250_assignmentPackage) EPackage.Registry.INSTANCE
					.getEPackage(Tdt4250_assignmentPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredTdt4250_assignmentPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		Tdt4250_assignmentPackageImpl theTdt4250_assignmentPackage = registeredTdt4250_assignmentPackage instanceof Tdt4250_assignmentPackageImpl
				? (Tdt4250_assignmentPackageImpl) registeredTdt4250_assignmentPackage
				: new Tdt4250_assignmentPackageImpl();

		isInited = true;

		// Create package meta-data objects
		theTdt4250_assignmentPackage.createPackageContents();

		// Initialize created meta-data
		theTdt4250_assignmentPackage.initializePackageContents();

		// Register package validator
		EValidator.Registry.INSTANCE.put(theTdt4250_assignmentPackage, new EValidator.Descriptor() {
			public EValidator getEValidator() {
				return Tdt4250_assignmentValidator.INSTANCE;
			}
		});

		// Mark meta-data to indicate it can't be changed
		theTdt4250_assignmentPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(Tdt4250_assignmentPackage.eNS_URI, theTdt4250_assignmentPackage);
		return theTdt4250_assignmentPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCourse() {
		return courseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCourse_CourseInstances() {
		return (EReference) courseEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCourse_Code() {
		return (EAttribute) courseEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCourse_Department() {
		return (EReference) courseEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCourse_Recommended() {
		return (EReference) courseEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCourse_Required() {
		return (EReference) courseEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCourseInstance() {
		return courseInstanceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCourseInstance_Evaluations() {
		return (EReference) courseInstanceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCourseInstance_TimeTable() {
		return (EReference) courseInstanceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCourseInstance_Roles() {
		return (EReference) courseInstanceEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCourseInstance_Content() {
		return (EAttribute) courseInstanceEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCourseInstance_Credits() {
		return (EAttribute) courseInstanceEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCourseInstance_StudyPrograms() {
		return (EAttribute) courseInstanceEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCourseInstance_LectureHours() {
		return (EAttribute) courseInstanceEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCourseInstance_LabHours() {
		return (EAttribute) courseInstanceEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCourseInstance_Year() {
		return (EAttribute) courseInstanceEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCourseInstance_Semester() {
		return (EAttribute) courseInstanceEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCourseInstance_Course() {
		return (EReference) courseInstanceEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCourse_CreditReductions() {
		return (EReference) courseEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getReduction() {
		return reductionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getReduction_Reduction() {
		return (EAttribute) reductionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getReduction_From() {
		return (EAttribute) reductionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getReduction_To() {
		return (EAttribute) reductionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getReduction_Course() {
		return (EReference) reductionEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getReduction_ReductionCourse() {
		return (EAttribute) reductionEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRole() {
		return roleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRole_Staff() {
		return (EReference) roleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRole_RoleType() {
		return (EAttribute) roleEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRole_Course() {
		return (EReference) roleEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDepartment() {
		return departmentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDepartment_Courses() {
		return (EReference) departmentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDepartment_Staff() {
		return (EReference) departmentEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPerson() {
		return personEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPerson_Department() {
		return (EReference) personEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPerson_Roles() {
		return (EReference) personEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEvaluation() {
		return evaluationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEvaluation_EvalType() {
		return (EAttribute) evaluationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEvaluation_Percentage() {
		return (EAttribute) evaluationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEvaluation_CourseInstance() {
		return (EReference) evaluationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEvaluation_Required() {
		return (EAttribute) evaluationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTimeTable() {
		return timeTableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTimeTable_Course() {
		return (EReference) timeTableEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTimeTable_Schedule() {
		return (EReference) timeTableEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getScheduleSlot() {
		return scheduleSlotEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getScheduleSlot_Type() {
		return (EAttribute) scheduleSlotEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScheduleSlot_TimeTable() {
		return (EReference) scheduleSlotEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getScheduleSlot_Day() {
		return (EAttribute) scheduleSlotEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getScheduleSlot_From() {
		return (EAttribute) scheduleSlotEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getScheduleSlot_To() {
		return (EAttribute) scheduleSlotEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getScheduleSlot_Room() {
		return (EAttribute) scheduleSlotEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getScheduleSlot_StudyPrograms() {
		return (EAttribute) scheduleSlotEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNamed() {
		return namedEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNamed_Name() {
		return (EAttribute) namedEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getSemesterType() {
		return semesterTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getCourseCode() {
		return courseCodeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getStudyProgramCode() {
		return studyProgramCodeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getRoom() {
		return roomEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getScheduleSlotType() {
		return scheduleSlotTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getDay() {
		return dayEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getEvaluationType() {
		return evaluationTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getRoleType() {
		return roleTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Tdt4250_assignmentFactory getTdt4250_assignmentFactory() {
		return (Tdt4250_assignmentFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		courseEClass = createEClass(COURSE);
		createEReference(courseEClass, COURSE__COURSE_INSTANCES);
		createEAttribute(courseEClass, COURSE__CODE);
		createEReference(courseEClass, COURSE__DEPARTMENT);
		createEReference(courseEClass, COURSE__CREDIT_REDUCTIONS);
		createEReference(courseEClass, COURSE__RECOMMENDED);
		createEReference(courseEClass, COURSE__REQUIRED);

		courseInstanceEClass = createEClass(COURSE_INSTANCE);
		createEReference(courseInstanceEClass, COURSE_INSTANCE__EVALUATIONS);
		createEReference(courseInstanceEClass, COURSE_INSTANCE__TIME_TABLE);
		createEReference(courseInstanceEClass, COURSE_INSTANCE__ROLES);
		createEAttribute(courseInstanceEClass, COURSE_INSTANCE__CONTENT);
		createEAttribute(courseInstanceEClass, COURSE_INSTANCE__CREDITS);
		createEAttribute(courseInstanceEClass, COURSE_INSTANCE__STUDY_PROGRAMS);
		createEAttribute(courseInstanceEClass, COURSE_INSTANCE__LECTURE_HOURS);
		createEAttribute(courseInstanceEClass, COURSE_INSTANCE__LAB_HOURS);
		createEAttribute(courseInstanceEClass, COURSE_INSTANCE__YEAR);
		createEAttribute(courseInstanceEClass, COURSE_INSTANCE__SEMESTER);
		createEReference(courseInstanceEClass, COURSE_INSTANCE__COURSE);

		reductionEClass = createEClass(REDUCTION);
		createEAttribute(reductionEClass, REDUCTION__REDUCTION);
		createEAttribute(reductionEClass, REDUCTION__FROM);
		createEAttribute(reductionEClass, REDUCTION__TO);
		createEReference(reductionEClass, REDUCTION__COURSE);
		createEAttribute(reductionEClass, REDUCTION__REDUCTION_COURSE);

		roleEClass = createEClass(ROLE);
		createEReference(roleEClass, ROLE__STAFF);
		createEAttribute(roleEClass, ROLE__ROLE_TYPE);
		createEReference(roleEClass, ROLE__COURSE);

		departmentEClass = createEClass(DEPARTMENT);
		createEReference(departmentEClass, DEPARTMENT__COURSES);
		createEReference(departmentEClass, DEPARTMENT__STAFF);

		personEClass = createEClass(PERSON);
		createEReference(personEClass, PERSON__DEPARTMENT);
		createEReference(personEClass, PERSON__ROLES);

		evaluationEClass = createEClass(EVALUATION);
		createEAttribute(evaluationEClass, EVALUATION__EVAL_TYPE);
		createEAttribute(evaluationEClass, EVALUATION__PERCENTAGE);
		createEReference(evaluationEClass, EVALUATION__COURSE_INSTANCE);
		createEAttribute(evaluationEClass, EVALUATION__REQUIRED);

		timeTableEClass = createEClass(TIME_TABLE);
		createEReference(timeTableEClass, TIME_TABLE__COURSE);
		createEReference(timeTableEClass, TIME_TABLE__SCHEDULE);

		scheduleSlotEClass = createEClass(SCHEDULE_SLOT);
		createEAttribute(scheduleSlotEClass, SCHEDULE_SLOT__TYPE);
		createEReference(scheduleSlotEClass, SCHEDULE_SLOT__TIME_TABLE);
		createEAttribute(scheduleSlotEClass, SCHEDULE_SLOT__DAY);
		createEAttribute(scheduleSlotEClass, SCHEDULE_SLOT__FROM);
		createEAttribute(scheduleSlotEClass, SCHEDULE_SLOT__TO);
		createEAttribute(scheduleSlotEClass, SCHEDULE_SLOT__ROOM);
		createEAttribute(scheduleSlotEClass, SCHEDULE_SLOT__STUDY_PROGRAMS);

		namedEClass = createEClass(NAMED);
		createEAttribute(namedEClass, NAMED__NAME);

		// Create enums
		semesterTypeEEnum = createEEnum(SEMESTER_TYPE);
		courseCodeEEnum = createEEnum(COURSE_CODE);
		roleTypeEEnum = createEEnum(ROLE_TYPE);
		evaluationTypeEEnum = createEEnum(EVALUATION_TYPE);
		studyProgramCodeEEnum = createEEnum(STUDY_PROGRAM_CODE);
		roomEEnum = createEEnum(ROOM);
		scheduleSlotTypeEEnum = createEEnum(SCHEDULE_SLOT_TYPE);
		dayEEnum = createEEnum(DAY);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		courseEClass.getESuperTypes().add(this.getNamed());
		departmentEClass.getESuperTypes().add(this.getNamed());
		personEClass.getESuperTypes().add(this.getNamed());

		// Initialize classes, features, and operations; add parameters
		initEClass(courseEClass, Course.class, "Course", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCourse_CourseInstances(), this.getCourseInstance(), this.getCourseInstance_Course(),
				"courseInstances", null, 1, -1, Course.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCourse_Code(), this.getCourseCode(), "code", null, 1, 1, Course.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCourse_Department(), this.getDepartment(), this.getDepartment_Courses(), "department", null,
				0, 1, Course.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCourse_CreditReductions(), this.getReduction(), this.getReduction_Course(),
				"creditReductions", null, 0, -1, Course.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCourse_Recommended(), this.getCourse(), null, "recommended", null, 0, -1, Course.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCourse_Required(), this.getCourse(), null, "required", null, 0, -1, Course.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(courseInstanceEClass, CourseInstance.class, "CourseInstance", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCourseInstance_Evaluations(), this.getEvaluation(), this.getEvaluation_CourseInstance(),
				"evaluations", null, 1, -1, CourseInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getCourseInstance_TimeTable(), this.getTimeTable(), this.getTimeTable_Course(), "timeTable",
				null, 1, 1, CourseInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCourseInstance_Roles(), this.getRole(), this.getRole_Course(), "roles", null, 1, -1,
				CourseInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCourseInstance_Content(), ecorePackage.getEString(), "content", null, 0, 1,
				CourseInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getCourseInstance_Credits(), ecorePackage.getEDouble(), "credits", "7.5", 1, 1,
				CourseInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getCourseInstance_StudyPrograms(), this.getStudyProgramCode(), "studyPrograms", null, 0, -1,
				CourseInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getCourseInstance_LectureHours(), ecorePackage.getEInt(), "lectureHours", null, 0, 1,
				CourseInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getCourseInstance_LabHours(), ecorePackage.getEInt(), "labHours", null, 0, 1,
				CourseInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getCourseInstance_Year(), ecorePackage.getEInt(), "year", null, 1, 1, CourseInstance.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCourseInstance_Semester(), this.getSemesterType(), "semester", null, 1, 1,
				CourseInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEReference(getCourseInstance_Course(), this.getCourse(), this.getCourse_CourseInstances(), "course", null,
				1, 1, CourseInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(reductionEClass, Reduction.class, "Reduction", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getReduction_Reduction(), ecorePackage.getEDouble(), "reduction", null, 0, 1, Reduction.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getReduction_From(), ecorePackage.getEDate(), "from", null, 0, 1, Reduction.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getReduction_To(), ecorePackage.getEDate(), "to", null, 0, 1, Reduction.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getReduction_Course(), this.getCourse(), this.getCourse_CreditReductions(), "course", null, 0, 1,
				Reduction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getReduction_ReductionCourse(), this.getCourseCode(), "reductionCourse", null, 0, 1,
				Reduction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(roleEClass, Role.class, "Role", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRole_Staff(), this.getPerson(), this.getPerson_Roles(), "staff", null, 1, 1, Role.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRole_RoleType(), this.getRoleType(), "roleType", "COORDINATOR", 0, 1, Role.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				!IS_ORDERED);
		initEReference(getRole_Course(), this.getCourseInstance(), this.getCourseInstance_Roles(), "course", null, 1, 1,
				Role.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(departmentEClass, Department.class, "Department", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDepartment_Courses(), this.getCourse(), this.getCourse_Department(), "courses", null, 0, -1,
				Department.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDepartment_Staff(), this.getPerson(), this.getPerson_Department(), "staff", null, 0, -1,
				Department.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(personEClass, Person.class, "Person", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPerson_Department(), this.getDepartment(), this.getDepartment_Staff(), "department", null, 1,
				1, Person.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPerson_Roles(), this.getRole(), this.getRole_Staff(), "roles", null, 0, -1, Person.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(evaluationEClass, Evaluation.class, "Evaluation", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getEvaluation_EvalType(), this.getEvaluationType(), "evalType", null, 0, 1, Evaluation.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEvaluation_Percentage(), ecorePackage.getEInt(), "percentage", null, 0, 1, Evaluation.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEvaluation_CourseInstance(), this.getCourseInstance(), this.getCourseInstance_Evaluations(),
				"courseInstance", null, 1, 1, Evaluation.class, !IS_TRANSIENT, !IS_VOLATILE, !IS_CHANGEABLE,
				!IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEvaluation_Required(), ecorePackage.getEBoolean(), "required", null, 0, 1, Evaluation.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(timeTableEClass, TimeTable.class, "TimeTable", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTimeTable_Course(), this.getCourseInstance(), this.getCourseInstance_TimeTable(), "course",
				null, 0, 1, TimeTable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTimeTable_Schedule(), this.getScheduleSlot(), this.getScheduleSlot_TimeTable(), "schedule",
				null, 0, -1, TimeTable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(scheduleSlotEClass, ScheduleSlot.class, "ScheduleSlot", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getScheduleSlot_Type(), this.getScheduleSlotType(), "type", null, 0, 1, ScheduleSlot.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getScheduleSlot_TimeTable(), this.getTimeTable(), this.getTimeTable_Schedule(), "timeTable",
				null, 0, 1, ScheduleSlot.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getScheduleSlot_Day(), this.getDay(), "day", null, 0, 1, ScheduleSlot.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getScheduleSlot_From(), ecorePackage.getEInt(), "from", null, 0, 1, ScheduleSlot.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getScheduleSlot_To(), ecorePackage.getEInt(), "to", null, 0, 1, ScheduleSlot.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getScheduleSlot_Room(), this.getRoom(), "room", null, 0, 1, ScheduleSlot.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getScheduleSlot_StudyPrograms(), this.getStudyProgramCode(), "studyPrograms", null, 0, -1,
				ScheduleSlot.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(namedEClass, Named.class, "Named", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getNamed_Name(), ecorePackage.getEString(), "name", null, 0, 1, Named.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(semesterTypeEEnum, SemesterType.class, "SemesterType");
		addEEnumLiteral(semesterTypeEEnum, SemesterType.AUTUMN);
		addEEnumLiteral(semesterTypeEEnum, SemesterType.SPRING);

		initEEnum(courseCodeEEnum, CourseCode.class, "CourseCode");
		addEEnumLiteral(courseCodeEEnum, CourseCode.TDT4100);
		addEEnumLiteral(courseCodeEEnum, CourseCode.TDT4250);
		addEEnumLiteral(courseCodeEEnum, CourseCode.TDT4102);

		initEEnum(roleTypeEEnum, RoleType.class, "RoleType");
		addEEnumLiteral(roleTypeEEnum, RoleType.LECTURER);
		addEEnumLiteral(roleTypeEEnum, RoleType.COORDINATOR);

		initEEnum(evaluationTypeEEnum, EvaluationType.class, "EvaluationType");
		addEEnumLiteral(evaluationTypeEEnum, EvaluationType.WORK);
		addEEnumLiteral(evaluationTypeEEnum, EvaluationType.WRITTEN_EXAM);

		initEEnum(studyProgramCodeEEnum, StudyProgramCode.class, "StudyProgramCode");
		addEEnumLiteral(studyProgramCodeEEnum, StudyProgramCode.MTDT);
		addEEnumLiteral(studyProgramCodeEEnum, StudyProgramCode.BIT);
		addEEnumLiteral(studyProgramCodeEEnum, StudyProgramCode.OTHER);

		initEEnum(roomEEnum, Room.class, "Room");
		addEEnumLiteral(roomEEnum, Room.K5);
		addEEnumLiteral(roomEEnum, Room.A34);
		addEEnumLiteral(roomEEnum, Room.F6);
		addEEnumLiteral(roomEEnum, Room.F1);

		initEEnum(scheduleSlotTypeEEnum, ScheduleSlotType.class, "ScheduleSlotType");
		addEEnumLiteral(scheduleSlotTypeEEnum, ScheduleSlotType.LECTURE);
		addEEnumLiteral(scheduleSlotTypeEEnum, ScheduleSlotType.EXERCISE);
		addEEnumLiteral(scheduleSlotTypeEEnum, ScheduleSlotType.LAB);

		initEEnum(dayEEnum, Day.class, "Day");
		addEEnumLiteral(dayEEnum, Day.MONDAY);
		addEEnumLiteral(dayEEnum, Day.TUESDAY);
		addEEnumLiteral(dayEEnum, Day.WEDNESSDAY);
		addEEnumLiteral(dayEEnum, Day.THURSDAY);
		addEEnumLiteral(dayEEnum, Day.FRIDAY);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/emf/2002/Ecore
		createEcoreAnnotations();
		// http://www.eclipse.org/emf/2002/Ecore/OCL
		createOCLAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEcoreAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore";
		addAnnotation(this, source, new String[] { "validationDelegates",
				"http://www.eclipse.org/acceleo/query/1.0 http://www.eclipse.org/emf/2002/Ecore/OCL" });
		addAnnotation(courseInstanceEClass, source,
				new String[] { "constraints", "validateEvaluation validateRoles validateHours" });
		addAnnotation(scheduleSlotEClass, source, new String[] { "constraints", "validateScheduleSlot" });
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore/OCL</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createOCLAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore/OCL";
		addAnnotation(courseInstanceEClass, source,
				new String[] { "validateEvaluation", "self.evaluations.percentage -> sum() = 100", "validateRoles",
						"self.roles -> select(r | r.roleType = RoleType::COORDINATOR) -> size() >= 1", "validateHours",
						null });
		addAnnotation(scheduleSlotEClass, source, new String[] { "validateScheduleSlot", "self.to > self.from" });
	}

} //Tdt4250_assignmentPackageImpl

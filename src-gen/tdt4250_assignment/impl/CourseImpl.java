/**
 */
package tdt4250_assignment.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

import tdt4250_assignment.Course;
import tdt4250_assignment.CourseCode;
import tdt4250_assignment.CourseInstance;
import tdt4250_assignment.Department;
import tdt4250_assignment.Reduction;
import tdt4250_assignment.Tdt4250_assignmentPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Course</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link tdt4250_assignment.impl.CourseImpl#getCourseInstances <em>Course Instances</em>}</li>
 *   <li>{@link tdt4250_assignment.impl.CourseImpl#getCode <em>Code</em>}</li>
 *   <li>{@link tdt4250_assignment.impl.CourseImpl#getDepartment <em>Department</em>}</li>
 *   <li>{@link tdt4250_assignment.impl.CourseImpl#getCreditReductions <em>Credit Reductions</em>}</li>
 *   <li>{@link tdt4250_assignment.impl.CourseImpl#getRecommended <em>Recommended</em>}</li>
 *   <li>{@link tdt4250_assignment.impl.CourseImpl#getRequired <em>Required</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CourseImpl extends NamedImpl implements Course {
	/**
	 * The cached value of the '{@link #getCourseInstances() <em>Course Instances</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCourseInstances()
	 * @generated
	 * @ordered
	 */
	protected EList<CourseInstance> courseInstances;

	/**
	 * The default value of the '{@link #getCode() <em>Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCode()
	 * @generated
	 * @ordered
	 */
	protected static final CourseCode CODE_EDEFAULT = CourseCode.TDT4100;

	/**
	 * The cached value of the '{@link #getCode() <em>Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCode()
	 * @generated
	 * @ordered
	 */
	protected CourseCode code = CODE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getCreditReductions() <em>Credit Reductions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCreditReductions()
	 * @generated
	 * @ordered
	 */
	protected EList<Reduction> creditReductions;

	/**
	 * The cached value of the '{@link #getRecommended() <em>Recommended</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRecommended()
	 * @generated
	 * @ordered
	 */
	protected EList<Course> recommended;

	/**
	 * The cached value of the '{@link #getRequired() <em>Required</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRequired()
	 * @generated
	 * @ordered
	 */
	protected EList<Course> required;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CourseImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Tdt4250_assignmentPackage.Literals.COURSE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CourseInstance> getCourseInstances() {
		if (courseInstances == null) {
			courseInstances = new EObjectContainmentWithInverseEList<CourseInstance>(CourseInstance.class, this,
					Tdt4250_assignmentPackage.COURSE__COURSE_INSTANCES,
					Tdt4250_assignmentPackage.COURSE_INSTANCE__COURSE);
		}
		return courseInstances;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CourseCode getCode() {
		return code;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCode(CourseCode newCode) {
		CourseCode oldCode = code;
		code = newCode == null ? CODE_EDEFAULT : newCode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Tdt4250_assignmentPackage.COURSE__CODE, oldCode,
					code));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Department getDepartment() {
		if (eContainerFeatureID() != Tdt4250_assignmentPackage.COURSE__DEPARTMENT)
			return null;
		return (Department) eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDepartment(Department newDepartment, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject) newDepartment, Tdt4250_assignmentPackage.COURSE__DEPARTMENT, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDepartment(Department newDepartment) {
		if (newDepartment != eInternalContainer()
				|| (eContainerFeatureID() != Tdt4250_assignmentPackage.COURSE__DEPARTMENT && newDepartment != null)) {
			if (EcoreUtil.isAncestor(this, newDepartment))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newDepartment != null)
				msgs = ((InternalEObject) newDepartment).eInverseAdd(this,
						Tdt4250_assignmentPackage.DEPARTMENT__COURSES, Department.class, msgs);
			msgs = basicSetDepartment(newDepartment, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Tdt4250_assignmentPackage.COURSE__DEPARTMENT,
					newDepartment, newDepartment));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Course> getRecommended() {
		if (recommended == null) {
			recommended = new EObjectResolvingEList<Course>(Course.class, this,
					Tdt4250_assignmentPackage.COURSE__RECOMMENDED);
		}
		return recommended;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Course> getRequired() {
		if (required == null) {
			required = new EObjectResolvingEList<Course>(Course.class, this,
					Tdt4250_assignmentPackage.COURSE__REQUIRED);
		}
		return required;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Reduction> getCreditReductions() {
		if (creditReductions == null) {
			creditReductions = new EObjectContainmentWithInverseEList<Reduction>(Reduction.class, this,
					Tdt4250_assignmentPackage.COURSE__CREDIT_REDUCTIONS, Tdt4250_assignmentPackage.REDUCTION__COURSE);
		}
		return creditReductions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case Tdt4250_assignmentPackage.COURSE__COURSE_INSTANCES:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getCourseInstances()).basicAdd(otherEnd, msgs);
		case Tdt4250_assignmentPackage.COURSE__DEPARTMENT:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetDepartment((Department) otherEnd, msgs);
		case Tdt4250_assignmentPackage.COURSE__CREDIT_REDUCTIONS:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getCreditReductions()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case Tdt4250_assignmentPackage.COURSE__COURSE_INSTANCES:
			return ((InternalEList<?>) getCourseInstances()).basicRemove(otherEnd, msgs);
		case Tdt4250_assignmentPackage.COURSE__DEPARTMENT:
			return basicSetDepartment(null, msgs);
		case Tdt4250_assignmentPackage.COURSE__CREDIT_REDUCTIONS:
			return ((InternalEList<?>) getCreditReductions()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
		case Tdt4250_assignmentPackage.COURSE__DEPARTMENT:
			return eInternalContainer().eInverseRemove(this, Tdt4250_assignmentPackage.DEPARTMENT__COURSES,
					Department.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Tdt4250_assignmentPackage.COURSE__COURSE_INSTANCES:
			return getCourseInstances();
		case Tdt4250_assignmentPackage.COURSE__CODE:
			return getCode();
		case Tdt4250_assignmentPackage.COURSE__DEPARTMENT:
			return getDepartment();
		case Tdt4250_assignmentPackage.COURSE__CREDIT_REDUCTIONS:
			return getCreditReductions();
		case Tdt4250_assignmentPackage.COURSE__RECOMMENDED:
			return getRecommended();
		case Tdt4250_assignmentPackage.COURSE__REQUIRED:
			return getRequired();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Tdt4250_assignmentPackage.COURSE__COURSE_INSTANCES:
			getCourseInstances().clear();
			getCourseInstances().addAll((Collection<? extends CourseInstance>) newValue);
			return;
		case Tdt4250_assignmentPackage.COURSE__CODE:
			setCode((CourseCode) newValue);
			return;
		case Tdt4250_assignmentPackage.COURSE__DEPARTMENT:
			setDepartment((Department) newValue);
			return;
		case Tdt4250_assignmentPackage.COURSE__CREDIT_REDUCTIONS:
			getCreditReductions().clear();
			getCreditReductions().addAll((Collection<? extends Reduction>) newValue);
			return;
		case Tdt4250_assignmentPackage.COURSE__RECOMMENDED:
			getRecommended().clear();
			getRecommended().addAll((Collection<? extends Course>) newValue);
			return;
		case Tdt4250_assignmentPackage.COURSE__REQUIRED:
			getRequired().clear();
			getRequired().addAll((Collection<? extends Course>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Tdt4250_assignmentPackage.COURSE__COURSE_INSTANCES:
			getCourseInstances().clear();
			return;
		case Tdt4250_assignmentPackage.COURSE__CODE:
			setCode(CODE_EDEFAULT);
			return;
		case Tdt4250_assignmentPackage.COURSE__DEPARTMENT:
			setDepartment((Department) null);
			return;
		case Tdt4250_assignmentPackage.COURSE__CREDIT_REDUCTIONS:
			getCreditReductions().clear();
			return;
		case Tdt4250_assignmentPackage.COURSE__RECOMMENDED:
			getRecommended().clear();
			return;
		case Tdt4250_assignmentPackage.COURSE__REQUIRED:
			getRequired().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Tdt4250_assignmentPackage.COURSE__COURSE_INSTANCES:
			return courseInstances != null && !courseInstances.isEmpty();
		case Tdt4250_assignmentPackage.COURSE__CODE:
			return code != CODE_EDEFAULT;
		case Tdt4250_assignmentPackage.COURSE__DEPARTMENT:
			return getDepartment() != null;
		case Tdt4250_assignmentPackage.COURSE__CREDIT_REDUCTIONS:
			return creditReductions != null && !creditReductions.isEmpty();
		case Tdt4250_assignmentPackage.COURSE__RECOMMENDED:
			return recommended != null && !recommended.isEmpty();
		case Tdt4250_assignmentPackage.COURSE__REQUIRED:
			return required != null && !required.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (code: ");
		result.append(code);
		result.append(')');
		return result.toString();
	}

} //CourseImpl

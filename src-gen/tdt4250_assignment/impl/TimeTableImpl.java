/**
 */
package tdt4250_assignment.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;
import tdt4250_assignment.CourseInstance;
import tdt4250_assignment.ScheduleSlot;
import tdt4250_assignment.Tdt4250_assignmentPackage;
import tdt4250_assignment.TimeTable;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Time Table</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link tdt4250_assignment.impl.TimeTableImpl#getCourse <em>Course</em>}</li>
 *   <li>{@link tdt4250_assignment.impl.TimeTableImpl#getSchedule <em>Schedule</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TimeTableImpl extends MinimalEObjectImpl.Container implements TimeTable {
	/**
	 * The cached value of the '{@link #getSchedule() <em>Schedule</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSchedule()
	 * @generated
	 * @ordered
	 */
	protected EList<ScheduleSlot> schedule;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TimeTableImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Tdt4250_assignmentPackage.Literals.TIME_TABLE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CourseInstance getCourse() {
		if (eContainerFeatureID() != Tdt4250_assignmentPackage.TIME_TABLE__COURSE)
			return null;
		return (CourseInstance) eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCourse(CourseInstance newCourse, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject) newCourse, Tdt4250_assignmentPackage.TIME_TABLE__COURSE, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCourse(CourseInstance newCourse) {
		if (newCourse != eInternalContainer()
				|| (eContainerFeatureID() != Tdt4250_assignmentPackage.TIME_TABLE__COURSE && newCourse != null)) {
			if (EcoreUtil.isAncestor(this, newCourse))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newCourse != null)
				msgs = ((InternalEObject) newCourse).eInverseAdd(this,
						Tdt4250_assignmentPackage.COURSE_INSTANCE__TIME_TABLE, CourseInstance.class, msgs);
			msgs = basicSetCourse(newCourse, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Tdt4250_assignmentPackage.TIME_TABLE__COURSE,
					newCourse, newCourse));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ScheduleSlot> getSchedule() {
		if (schedule == null) {
			schedule = new EObjectContainmentWithInverseEList<ScheduleSlot>(ScheduleSlot.class, this,
					Tdt4250_assignmentPackage.TIME_TABLE__SCHEDULE,
					Tdt4250_assignmentPackage.SCHEDULE_SLOT__TIME_TABLE);
		}
		return schedule;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case Tdt4250_assignmentPackage.TIME_TABLE__COURSE:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetCourse((CourseInstance) otherEnd, msgs);
		case Tdt4250_assignmentPackage.TIME_TABLE__SCHEDULE:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getSchedule()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case Tdt4250_assignmentPackage.TIME_TABLE__COURSE:
			return basicSetCourse(null, msgs);
		case Tdt4250_assignmentPackage.TIME_TABLE__SCHEDULE:
			return ((InternalEList<?>) getSchedule()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
		case Tdt4250_assignmentPackage.TIME_TABLE__COURSE:
			return eInternalContainer().eInverseRemove(this, Tdt4250_assignmentPackage.COURSE_INSTANCE__TIME_TABLE,
					CourseInstance.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Tdt4250_assignmentPackage.TIME_TABLE__COURSE:
			return getCourse();
		case Tdt4250_assignmentPackage.TIME_TABLE__SCHEDULE:
			return getSchedule();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Tdt4250_assignmentPackage.TIME_TABLE__COURSE:
			setCourse((CourseInstance) newValue);
			return;
		case Tdt4250_assignmentPackage.TIME_TABLE__SCHEDULE:
			getSchedule().clear();
			getSchedule().addAll((Collection<? extends ScheduleSlot>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Tdt4250_assignmentPackage.TIME_TABLE__COURSE:
			setCourse((CourseInstance) null);
			return;
		case Tdt4250_assignmentPackage.TIME_TABLE__SCHEDULE:
			getSchedule().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Tdt4250_assignmentPackage.TIME_TABLE__COURSE:
			return getCourse() != null;
		case Tdt4250_assignmentPackage.TIME_TABLE__SCHEDULE:
			return schedule != null && !schedule.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //TimeTableImpl

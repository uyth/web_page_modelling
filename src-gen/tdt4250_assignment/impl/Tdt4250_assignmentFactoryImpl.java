/**
 */
package tdt4250_assignment.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import tdt4250_assignment.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Tdt4250_assignmentFactoryImpl extends EFactoryImpl implements Tdt4250_assignmentFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Tdt4250_assignmentFactory init() {
		try {
			Tdt4250_assignmentFactory theTdt4250_assignmentFactory = (Tdt4250_assignmentFactory) EPackage.Registry.INSTANCE
					.getEFactory(Tdt4250_assignmentPackage.eNS_URI);
			if (theTdt4250_assignmentFactory != null) {
				return theTdt4250_assignmentFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new Tdt4250_assignmentFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Tdt4250_assignmentFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case Tdt4250_assignmentPackage.COURSE:
			return createCourse();
		case Tdt4250_assignmentPackage.COURSE_INSTANCE:
			return createCourseInstance();
		case Tdt4250_assignmentPackage.REDUCTION:
			return createReduction();
		case Tdt4250_assignmentPackage.ROLE:
			return createRole();
		case Tdt4250_assignmentPackage.DEPARTMENT:
			return createDepartment();
		case Tdt4250_assignmentPackage.PERSON:
			return createPerson();
		case Tdt4250_assignmentPackage.EVALUATION:
			return createEvaluation();
		case Tdt4250_assignmentPackage.TIME_TABLE:
			return createTimeTable();
		case Tdt4250_assignmentPackage.SCHEDULE_SLOT:
			return createScheduleSlot();
		case Tdt4250_assignmentPackage.NAMED:
			return createNamed();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
		case Tdt4250_assignmentPackage.SEMESTER_TYPE:
			return createSemesterTypeFromString(eDataType, initialValue);
		case Tdt4250_assignmentPackage.COURSE_CODE:
			return createCourseCodeFromString(eDataType, initialValue);
		case Tdt4250_assignmentPackage.ROLE_TYPE:
			return createRoleTypeFromString(eDataType, initialValue);
		case Tdt4250_assignmentPackage.EVALUATION_TYPE:
			return createEvaluationTypeFromString(eDataType, initialValue);
		case Tdt4250_assignmentPackage.STUDY_PROGRAM_CODE:
			return createStudyProgramCodeFromString(eDataType, initialValue);
		case Tdt4250_assignmentPackage.ROOM:
			return createRoomFromString(eDataType, initialValue);
		case Tdt4250_assignmentPackage.SCHEDULE_SLOT_TYPE:
			return createScheduleSlotTypeFromString(eDataType, initialValue);
		case Tdt4250_assignmentPackage.DAY:
			return createDayFromString(eDataType, initialValue);
		default:
			throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
		case Tdt4250_assignmentPackage.SEMESTER_TYPE:
			return convertSemesterTypeToString(eDataType, instanceValue);
		case Tdt4250_assignmentPackage.COURSE_CODE:
			return convertCourseCodeToString(eDataType, instanceValue);
		case Tdt4250_assignmentPackage.ROLE_TYPE:
			return convertRoleTypeToString(eDataType, instanceValue);
		case Tdt4250_assignmentPackage.EVALUATION_TYPE:
			return convertEvaluationTypeToString(eDataType, instanceValue);
		case Tdt4250_assignmentPackage.STUDY_PROGRAM_CODE:
			return convertStudyProgramCodeToString(eDataType, instanceValue);
		case Tdt4250_assignmentPackage.ROOM:
			return convertRoomToString(eDataType, instanceValue);
		case Tdt4250_assignmentPackage.SCHEDULE_SLOT_TYPE:
			return convertScheduleSlotTypeToString(eDataType, instanceValue);
		case Tdt4250_assignmentPackage.DAY:
			return convertDayToString(eDataType, instanceValue);
		default:
			throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Course createCourse() {
		CourseImpl course = new CourseImpl();
		return course;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CourseInstance createCourseInstance() {
		CourseInstanceImpl courseInstance = new CourseInstanceImpl();
		return courseInstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Reduction createReduction() {
		ReductionImpl reduction = new ReductionImpl();
		return reduction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Role createRole() {
		RoleImpl role = new RoleImpl();
		return role;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Department createDepartment() {
		DepartmentImpl department = new DepartmentImpl();
		return department;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Person createPerson() {
		PersonImpl person = new PersonImpl();
		return person;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Evaluation createEvaluation() {
		EvaluationImpl evaluation = new EvaluationImpl();
		return evaluation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimeTable createTimeTable() {
		TimeTableImpl timeTable = new TimeTableImpl();
		return timeTable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScheduleSlot createScheduleSlot() {
		ScheduleSlotImpl scheduleSlot = new ScheduleSlotImpl();
		return scheduleSlot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Named createNamed() {
		NamedImpl named = new NamedImpl();
		return named;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SemesterType createSemesterTypeFromString(EDataType eDataType, String initialValue) {
		SemesterType result = SemesterType.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertSemesterTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CourseCode createCourseCodeFromString(EDataType eDataType, String initialValue) {
		CourseCode result = CourseCode.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertCourseCodeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StudyProgramCode createStudyProgramCodeFromString(EDataType eDataType, String initialValue) {
		StudyProgramCode result = StudyProgramCode.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertStudyProgramCodeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Room createRoomFromString(EDataType eDataType, String initialValue) {
		Room result = Room.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertRoomToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScheduleSlotType createScheduleSlotTypeFromString(EDataType eDataType, String initialValue) {
		ScheduleSlotType result = ScheduleSlotType.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertScheduleSlotTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Day createDayFromString(EDataType eDataType, String initialValue) {
		Day result = Day.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDayToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EvaluationType createEvaluationTypeFromString(EDataType eDataType, String initialValue) {
		EvaluationType result = EvaluationType.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertEvaluationTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoleType createRoleTypeFromString(EDataType eDataType, String initialValue) {
		RoleType result = RoleType.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertRoleTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Tdt4250_assignmentPackage getTdt4250_assignmentPackage() {
		return (Tdt4250_assignmentPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static Tdt4250_assignmentPackage getPackage() {
		return Tdt4250_assignmentPackage.eINSTANCE;
	}

} //Tdt4250_assignmentFactoryImpl

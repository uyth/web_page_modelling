/**
 */
package tdt4250_assignment.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

import tdt4250_assignment.Course;
import tdt4250_assignment.CourseInstance;
import tdt4250_assignment.Evaluation;
import tdt4250_assignment.Role;
import tdt4250_assignment.SemesterType;
import tdt4250_assignment.StudyProgramCode;
import tdt4250_assignment.Tdt4250_assignmentPackage;
import tdt4250_assignment.TimeTable;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Course Instance</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link tdt4250_assignment.impl.CourseInstanceImpl#getEvaluations <em>Evaluations</em>}</li>
 *   <li>{@link tdt4250_assignment.impl.CourseInstanceImpl#getTimeTable <em>Time Table</em>}</li>
 *   <li>{@link tdt4250_assignment.impl.CourseInstanceImpl#getRoles <em>Roles</em>}</li>
 *   <li>{@link tdt4250_assignment.impl.CourseInstanceImpl#getContent <em>Content</em>}</li>
 *   <li>{@link tdt4250_assignment.impl.CourseInstanceImpl#getCredits <em>Credits</em>}</li>
 *   <li>{@link tdt4250_assignment.impl.CourseInstanceImpl#getStudyPrograms <em>Study Programs</em>}</li>
 *   <li>{@link tdt4250_assignment.impl.CourseInstanceImpl#getLectureHours <em>Lecture Hours</em>}</li>
 *   <li>{@link tdt4250_assignment.impl.CourseInstanceImpl#getLabHours <em>Lab Hours</em>}</li>
 *   <li>{@link tdt4250_assignment.impl.CourseInstanceImpl#getYear <em>Year</em>}</li>
 *   <li>{@link tdt4250_assignment.impl.CourseInstanceImpl#getSemester <em>Semester</em>}</li>
 *   <li>{@link tdt4250_assignment.impl.CourseInstanceImpl#getCourse <em>Course</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CourseInstanceImpl extends MinimalEObjectImpl.Container implements CourseInstance {
	/**
	 * The cached value of the '{@link #getEvaluations() <em>Evaluations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEvaluations()
	 * @generated
	 * @ordered
	 */
	protected EList<Evaluation> evaluations;

	/**
	 * The cached value of the '{@link #getTimeTable() <em>Time Table</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimeTable()
	 * @generated
	 * @ordered
	 */
	protected TimeTable timeTable;

	/**
	 * The cached value of the '{@link #getRoles() <em>Roles</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoles()
	 * @generated
	 * @ordered
	 */
	protected EList<Role> roles;

	/**
	 * The default value of the '{@link #getContent() <em>Content</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContent()
	 * @generated
	 * @ordered
	 */
	protected static final String CONTENT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getContent() <em>Content</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContent()
	 * @generated
	 * @ordered
	 */
	protected String content = CONTENT_EDEFAULT;

	/**
	 * The default value of the '{@link #getCredits() <em>Credits</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCredits()
	 * @generated
	 * @ordered
	 */
	protected static final double CREDITS_EDEFAULT = 7.5;

	/**
	 * The cached value of the '{@link #getCredits() <em>Credits</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCredits()
	 * @generated
	 * @ordered
	 */
	protected double credits = CREDITS_EDEFAULT;

	/**
	 * The cached value of the '{@link #getStudyPrograms() <em>Study Programs</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStudyPrograms()
	 * @generated
	 * @ordered
	 */
	protected EList<StudyProgramCode> studyPrograms;

	/**
	 * The default value of the '{@link #getLectureHours() <em>Lecture Hours</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLectureHours()
	 * @generated
	 * @ordered
	 */
	protected static final int LECTURE_HOURS_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getLectureHours() <em>Lecture Hours</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLectureHours()
	 * @generated
	 * @ordered
	 */
	protected int lectureHours = LECTURE_HOURS_EDEFAULT;

	/**
	 * The default value of the '{@link #getLabHours() <em>Lab Hours</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabHours()
	 * @generated
	 * @ordered
	 */
	protected static final int LAB_HOURS_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getLabHours() <em>Lab Hours</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabHours()
	 * @generated
	 * @ordered
	 */
	protected int labHours = LAB_HOURS_EDEFAULT;

	/**
	 * The default value of the '{@link #getYear() <em>Year</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getYear()
	 * @generated
	 * @ordered
	 */
	protected static final int YEAR_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getYear() <em>Year</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getYear()
	 * @generated
	 * @ordered
	 */
	protected int year = YEAR_EDEFAULT;

	/**
	 * The default value of the '{@link #getSemester() <em>Semester</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSemester()
	 * @generated
	 * @ordered
	 */
	protected static final SemesterType SEMESTER_EDEFAULT = SemesterType.AUTUMN;

	/**
	 * The cached value of the '{@link #getSemester() <em>Semester</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSemester()
	 * @generated
	 * @ordered
	 */
	protected SemesterType semester = SEMESTER_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CourseInstanceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Tdt4250_assignmentPackage.Literals.COURSE_INSTANCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Evaluation> getEvaluations() {
		if (evaluations == null) {
			evaluations = new EObjectContainmentWithInverseEList<Evaluation>(Evaluation.class, this,
					Tdt4250_assignmentPackage.COURSE_INSTANCE__EVALUATIONS,
					Tdt4250_assignmentPackage.EVALUATION__COURSE_INSTANCE);
		}
		return evaluations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimeTable getTimeTable() {
		return timeTable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTimeTable(TimeTable newTimeTable, NotificationChain msgs) {
		TimeTable oldTimeTable = timeTable;
		timeTable = newTimeTable;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					Tdt4250_assignmentPackage.COURSE_INSTANCE__TIME_TABLE, oldTimeTable, newTimeTable);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTimeTable(TimeTable newTimeTable) {
		if (newTimeTable != timeTable) {
			NotificationChain msgs = null;
			if (timeTable != null)
				msgs = ((InternalEObject) timeTable).eInverseRemove(this, Tdt4250_assignmentPackage.TIME_TABLE__COURSE,
						TimeTable.class, msgs);
			if (newTimeTable != null)
				msgs = ((InternalEObject) newTimeTable).eInverseAdd(this, Tdt4250_assignmentPackage.TIME_TABLE__COURSE,
						TimeTable.class, msgs);
			msgs = basicSetTimeTable(newTimeTable, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Tdt4250_assignmentPackage.COURSE_INSTANCE__TIME_TABLE,
					newTimeTable, newTimeTable));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Role> getRoles() {
		if (roles == null) {
			roles = new EObjectContainmentWithInverseEList<Role>(Role.class, this,
					Tdt4250_assignmentPackage.COURSE_INSTANCE__ROLES, Tdt4250_assignmentPackage.ROLE__COURSE);
		}
		return roles;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getContent() {
		return content;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContent(String newContent) {
		String oldContent = content;
		content = newContent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Tdt4250_assignmentPackage.COURSE_INSTANCE__CONTENT,
					oldContent, content));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getCredits() {
		return credits;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCredits(double newCredits) {
		double oldCredits = credits;
		credits = newCredits;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Tdt4250_assignmentPackage.COURSE_INSTANCE__CREDITS,
					oldCredits, credits));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<StudyProgramCode> getStudyPrograms() {
		if (studyPrograms == null) {
			studyPrograms = new EDataTypeUniqueEList<StudyProgramCode>(StudyProgramCode.class, this,
					Tdt4250_assignmentPackage.COURSE_INSTANCE__STUDY_PROGRAMS);
		}
		return studyPrograms;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getLectureHours() {
		return lectureHours;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLectureHours(int newLectureHours) {
		int oldLectureHours = lectureHours;
		lectureHours = newLectureHours;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Tdt4250_assignmentPackage.COURSE_INSTANCE__LECTURE_HOURS, oldLectureHours, lectureHours));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getLabHours() {
		return labHours;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLabHours(int newLabHours) {
		int oldLabHours = labHours;
		labHours = newLabHours;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Tdt4250_assignmentPackage.COURSE_INSTANCE__LAB_HOURS,
					oldLabHours, labHours));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getYear() {
		return year;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setYear(int newYear) {
		int oldYear = year;
		year = newYear;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Tdt4250_assignmentPackage.COURSE_INSTANCE__YEAR,
					oldYear, year));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SemesterType getSemester() {
		return semester;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSemester(SemesterType newSemester) {
		SemesterType oldSemester = semester;
		semester = newSemester == null ? SEMESTER_EDEFAULT : newSemester;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Tdt4250_assignmentPackage.COURSE_INSTANCE__SEMESTER,
					oldSemester, semester));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Course getCourse() {
		if (eContainerFeatureID() != Tdt4250_assignmentPackage.COURSE_INSTANCE__COURSE)
			return null;
		return (Course) eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCourse(Course newCourse, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject) newCourse, Tdt4250_assignmentPackage.COURSE_INSTANCE__COURSE, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCourse(Course newCourse) {
		if (newCourse != eInternalContainer()
				|| (eContainerFeatureID() != Tdt4250_assignmentPackage.COURSE_INSTANCE__COURSE && newCourse != null)) {
			if (EcoreUtil.isAncestor(this, newCourse))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newCourse != null)
				msgs = ((InternalEObject) newCourse).eInverseAdd(this,
						Tdt4250_assignmentPackage.COURSE__COURSE_INSTANCES, Course.class, msgs);
			msgs = basicSetCourse(newCourse, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Tdt4250_assignmentPackage.COURSE_INSTANCE__COURSE,
					newCourse, newCourse));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case Tdt4250_assignmentPackage.COURSE_INSTANCE__EVALUATIONS:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getEvaluations()).basicAdd(otherEnd, msgs);
		case Tdt4250_assignmentPackage.COURSE_INSTANCE__TIME_TABLE:
			if (timeTable != null)
				msgs = ((InternalEObject) timeTable).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - Tdt4250_assignmentPackage.COURSE_INSTANCE__TIME_TABLE, null, msgs);
			return basicSetTimeTable((TimeTable) otherEnd, msgs);
		case Tdt4250_assignmentPackage.COURSE_INSTANCE__ROLES:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getRoles()).basicAdd(otherEnd, msgs);
		case Tdt4250_assignmentPackage.COURSE_INSTANCE__COURSE:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetCourse((Course) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case Tdt4250_assignmentPackage.COURSE_INSTANCE__EVALUATIONS:
			return ((InternalEList<?>) getEvaluations()).basicRemove(otherEnd, msgs);
		case Tdt4250_assignmentPackage.COURSE_INSTANCE__TIME_TABLE:
			return basicSetTimeTable(null, msgs);
		case Tdt4250_assignmentPackage.COURSE_INSTANCE__ROLES:
			return ((InternalEList<?>) getRoles()).basicRemove(otherEnd, msgs);
		case Tdt4250_assignmentPackage.COURSE_INSTANCE__COURSE:
			return basicSetCourse(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
		case Tdt4250_assignmentPackage.COURSE_INSTANCE__COURSE:
			return eInternalContainer().eInverseRemove(this, Tdt4250_assignmentPackage.COURSE__COURSE_INSTANCES,
					Course.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Tdt4250_assignmentPackage.COURSE_INSTANCE__EVALUATIONS:
			return getEvaluations();
		case Tdt4250_assignmentPackage.COURSE_INSTANCE__TIME_TABLE:
			return getTimeTable();
		case Tdt4250_assignmentPackage.COURSE_INSTANCE__ROLES:
			return getRoles();
		case Tdt4250_assignmentPackage.COURSE_INSTANCE__CONTENT:
			return getContent();
		case Tdt4250_assignmentPackage.COURSE_INSTANCE__CREDITS:
			return getCredits();
		case Tdt4250_assignmentPackage.COURSE_INSTANCE__STUDY_PROGRAMS:
			return getStudyPrograms();
		case Tdt4250_assignmentPackage.COURSE_INSTANCE__LECTURE_HOURS:
			return getLectureHours();
		case Tdt4250_assignmentPackage.COURSE_INSTANCE__LAB_HOURS:
			return getLabHours();
		case Tdt4250_assignmentPackage.COURSE_INSTANCE__YEAR:
			return getYear();
		case Tdt4250_assignmentPackage.COURSE_INSTANCE__SEMESTER:
			return getSemester();
		case Tdt4250_assignmentPackage.COURSE_INSTANCE__COURSE:
			return getCourse();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Tdt4250_assignmentPackage.COURSE_INSTANCE__EVALUATIONS:
			getEvaluations().clear();
			getEvaluations().addAll((Collection<? extends Evaluation>) newValue);
			return;
		case Tdt4250_assignmentPackage.COURSE_INSTANCE__TIME_TABLE:
			setTimeTable((TimeTable) newValue);
			return;
		case Tdt4250_assignmentPackage.COURSE_INSTANCE__ROLES:
			getRoles().clear();
			getRoles().addAll((Collection<? extends Role>) newValue);
			return;
		case Tdt4250_assignmentPackage.COURSE_INSTANCE__CONTENT:
			setContent((String) newValue);
			return;
		case Tdt4250_assignmentPackage.COURSE_INSTANCE__CREDITS:
			setCredits((Double) newValue);
			return;
		case Tdt4250_assignmentPackage.COURSE_INSTANCE__STUDY_PROGRAMS:
			getStudyPrograms().clear();
			getStudyPrograms().addAll((Collection<? extends StudyProgramCode>) newValue);
			return;
		case Tdt4250_assignmentPackage.COURSE_INSTANCE__LECTURE_HOURS:
			setLectureHours((Integer) newValue);
			return;
		case Tdt4250_assignmentPackage.COURSE_INSTANCE__LAB_HOURS:
			setLabHours((Integer) newValue);
			return;
		case Tdt4250_assignmentPackage.COURSE_INSTANCE__YEAR:
			setYear((Integer) newValue);
			return;
		case Tdt4250_assignmentPackage.COURSE_INSTANCE__SEMESTER:
			setSemester((SemesterType) newValue);
			return;
		case Tdt4250_assignmentPackage.COURSE_INSTANCE__COURSE:
			setCourse((Course) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Tdt4250_assignmentPackage.COURSE_INSTANCE__EVALUATIONS:
			getEvaluations().clear();
			return;
		case Tdt4250_assignmentPackage.COURSE_INSTANCE__TIME_TABLE:
			setTimeTable((TimeTable) null);
			return;
		case Tdt4250_assignmentPackage.COURSE_INSTANCE__ROLES:
			getRoles().clear();
			return;
		case Tdt4250_assignmentPackage.COURSE_INSTANCE__CONTENT:
			setContent(CONTENT_EDEFAULT);
			return;
		case Tdt4250_assignmentPackage.COURSE_INSTANCE__CREDITS:
			setCredits(CREDITS_EDEFAULT);
			return;
		case Tdt4250_assignmentPackage.COURSE_INSTANCE__STUDY_PROGRAMS:
			getStudyPrograms().clear();
			return;
		case Tdt4250_assignmentPackage.COURSE_INSTANCE__LECTURE_HOURS:
			setLectureHours(LECTURE_HOURS_EDEFAULT);
			return;
		case Tdt4250_assignmentPackage.COURSE_INSTANCE__LAB_HOURS:
			setLabHours(LAB_HOURS_EDEFAULT);
			return;
		case Tdt4250_assignmentPackage.COURSE_INSTANCE__YEAR:
			setYear(YEAR_EDEFAULT);
			return;
		case Tdt4250_assignmentPackage.COURSE_INSTANCE__SEMESTER:
			setSemester(SEMESTER_EDEFAULT);
			return;
		case Tdt4250_assignmentPackage.COURSE_INSTANCE__COURSE:
			setCourse((Course) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Tdt4250_assignmentPackage.COURSE_INSTANCE__EVALUATIONS:
			return evaluations != null && !evaluations.isEmpty();
		case Tdt4250_assignmentPackage.COURSE_INSTANCE__TIME_TABLE:
			return timeTable != null;
		case Tdt4250_assignmentPackage.COURSE_INSTANCE__ROLES:
			return roles != null && !roles.isEmpty();
		case Tdt4250_assignmentPackage.COURSE_INSTANCE__CONTENT:
			return CONTENT_EDEFAULT == null ? content != null : !CONTENT_EDEFAULT.equals(content);
		case Tdt4250_assignmentPackage.COURSE_INSTANCE__CREDITS:
			return credits != CREDITS_EDEFAULT;
		case Tdt4250_assignmentPackage.COURSE_INSTANCE__STUDY_PROGRAMS:
			return studyPrograms != null && !studyPrograms.isEmpty();
		case Tdt4250_assignmentPackage.COURSE_INSTANCE__LECTURE_HOURS:
			return lectureHours != LECTURE_HOURS_EDEFAULT;
		case Tdt4250_assignmentPackage.COURSE_INSTANCE__LAB_HOURS:
			return labHours != LAB_HOURS_EDEFAULT;
		case Tdt4250_assignmentPackage.COURSE_INSTANCE__YEAR:
			return year != YEAR_EDEFAULT;
		case Tdt4250_assignmentPackage.COURSE_INSTANCE__SEMESTER:
			return semester != SEMESTER_EDEFAULT;
		case Tdt4250_assignmentPackage.COURSE_INSTANCE__COURSE:
			return getCourse() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (content: ");
		result.append(content);
		result.append(", credits: ");
		result.append(credits);
		result.append(", studyPrograms: ");
		result.append(studyPrograms);
		result.append(", lectureHours: ");
		result.append(lectureHours);
		result.append(", labHours: ");
		result.append(labHours);
		result.append(", year: ");
		result.append(year);
		result.append(", semester: ");
		result.append(semester);
		result.append(')');
		return result.toString();
	}

} //CourseInstanceImpl

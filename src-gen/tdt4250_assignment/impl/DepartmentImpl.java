/**
 */
package tdt4250_assignment.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;

import tdt4250_assignment.Course;
import tdt4250_assignment.Department;
import tdt4250_assignment.Person;
import tdt4250_assignment.Tdt4250_assignmentPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Department</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link tdt4250_assignment.impl.DepartmentImpl#getCourses <em>Courses</em>}</li>
 *   <li>{@link tdt4250_assignment.impl.DepartmentImpl#getStaff <em>Staff</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DepartmentImpl extends NamedImpl implements Department {
	/**
	 * The cached value of the '{@link #getCourses() <em>Courses</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCourses()
	 * @generated
	 * @ordered
	 */
	protected EList<Course> courses;

	/**
	 * The cached value of the '{@link #getStaff() <em>Staff</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStaff()
	 * @generated
	 * @ordered
	 */
	protected EList<Person> staff;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DepartmentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Tdt4250_assignmentPackage.Literals.DEPARTMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Course> getCourses() {
		if (courses == null) {
			courses = new EObjectContainmentWithInverseEList<Course>(Course.class, this,
					Tdt4250_assignmentPackage.DEPARTMENT__COURSES, Tdt4250_assignmentPackage.COURSE__DEPARTMENT);
		}
		return courses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Person> getStaff() {
		if (staff == null) {
			staff = new EObjectContainmentWithInverseEList<Person>(Person.class, this,
					Tdt4250_assignmentPackage.DEPARTMENT__STAFF, Tdt4250_assignmentPackage.PERSON__DEPARTMENT);
		}
		return staff;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case Tdt4250_assignmentPackage.DEPARTMENT__COURSES:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getCourses()).basicAdd(otherEnd, msgs);
		case Tdt4250_assignmentPackage.DEPARTMENT__STAFF:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getStaff()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case Tdt4250_assignmentPackage.DEPARTMENT__COURSES:
			return ((InternalEList<?>) getCourses()).basicRemove(otherEnd, msgs);
		case Tdt4250_assignmentPackage.DEPARTMENT__STAFF:
			return ((InternalEList<?>) getStaff()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Tdt4250_assignmentPackage.DEPARTMENT__COURSES:
			return getCourses();
		case Tdt4250_assignmentPackage.DEPARTMENT__STAFF:
			return getStaff();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Tdt4250_assignmentPackage.DEPARTMENT__COURSES:
			getCourses().clear();
			getCourses().addAll((Collection<? extends Course>) newValue);
			return;
		case Tdt4250_assignmentPackage.DEPARTMENT__STAFF:
			getStaff().clear();
			getStaff().addAll((Collection<? extends Person>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Tdt4250_assignmentPackage.DEPARTMENT__COURSES:
			getCourses().clear();
			return;
		case Tdt4250_assignmentPackage.DEPARTMENT__STAFF:
			getStaff().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Tdt4250_assignmentPackage.DEPARTMENT__COURSES:
			return courses != null && !courses.isEmpty();
		case Tdt4250_assignmentPackage.DEPARTMENT__STAFF:
			return staff != null && !staff.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //DepartmentImpl

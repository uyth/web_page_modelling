/**
 */
package tdt4250_assignment.util;

import org.eclipse.emf.common.util.URI;

import org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl;

/**
 * <!-- begin-user-doc -->
 * The <b>Resource </b> associated with the package.
 * <!-- end-user-doc -->
 * @see tdt4250_assignment.util.Tdt4250_assignmentResourceFactoryImpl
 * @generated
 */
public class Tdt4250_assignmentResourceImpl extends XMIResourceImpl {
	/**
	 * Creates an instance of the resource.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param uri the URI of the new resource.
	 * @generated
	 */
	public Tdt4250_assignmentResourceImpl(URI uri) {
		super(uri);
	}

} //Tdt4250_assignmentResourceImpl

/**
 */
package tdt4250_assignment.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.EObjectValidator;

import tdt4250_assignment.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see tdt4250_assignment.Tdt4250_assignmentPackage
 * @generated
 */
public class Tdt4250_assignmentValidator extends EObjectValidator {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final Tdt4250_assignmentValidator INSTANCE = new Tdt4250_assignmentValidator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "tdt4250_assignment";

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 0;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Tdt4250_assignmentValidator() {
		super();
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
		return Tdt4250_assignmentPackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		switch (classifierID) {
		case Tdt4250_assignmentPackage.COURSE:
			return validateCourse((Course) value, diagnostics, context);
		case Tdt4250_assignmentPackage.COURSE_INSTANCE:
			return validateCourseInstance((CourseInstance) value, diagnostics, context);
		case Tdt4250_assignmentPackage.REDUCTION:
			return validateReduction((Reduction) value, diagnostics, context);
		case Tdt4250_assignmentPackage.ROLE:
			return validateRole((Role) value, diagnostics, context);
		case Tdt4250_assignmentPackage.DEPARTMENT:
			return validateDepartment((Department) value, diagnostics, context);
		case Tdt4250_assignmentPackage.PERSON:
			return validatePerson((Person) value, diagnostics, context);
		case Tdt4250_assignmentPackage.EVALUATION:
			return validateEvaluation((Evaluation) value, diagnostics, context);
		case Tdt4250_assignmentPackage.TIME_TABLE:
			return validateTimeTable((TimeTable) value, diagnostics, context);
		case Tdt4250_assignmentPackage.SCHEDULE_SLOT:
			return validateScheduleSlot((ScheduleSlot) value, diagnostics, context);
		case Tdt4250_assignmentPackage.NAMED:
			return validateNamed((Named) value, diagnostics, context);
		case Tdt4250_assignmentPackage.SEMESTER_TYPE:
			return validateSemesterType((SemesterType) value, diagnostics, context);
		case Tdt4250_assignmentPackage.COURSE_CODE:
			return validateCourseCode((CourseCode) value, diagnostics, context);
		case Tdt4250_assignmentPackage.ROLE_TYPE:
			return validateRoleType((RoleType) value, diagnostics, context);
		case Tdt4250_assignmentPackage.EVALUATION_TYPE:
			return validateEvaluationType((EvaluationType) value, diagnostics, context);
		case Tdt4250_assignmentPackage.STUDY_PROGRAM_CODE:
			return validateStudyProgramCode((StudyProgramCode) value, diagnostics, context);
		case Tdt4250_assignmentPackage.ROOM:
			return validateRoom((Room) value, diagnostics, context);
		case Tdt4250_assignmentPackage.SCHEDULE_SLOT_TYPE:
			return validateScheduleSlotType((ScheduleSlotType) value, diagnostics, context);
		case Tdt4250_assignmentPackage.DAY:
			return validateDay((Day) value, diagnostics, context);
		default:
			return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCourse(Course course, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(course, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCourseInstance(CourseInstance courseInstance, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		if (!validate_NoCircularContainment(courseInstance, diagnostics, context))
			return false;
		boolean result = validate_EveryMultiplicityConforms(courseInstance, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryDataValueConforms(courseInstance, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryReferenceIsContained(courseInstance, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryBidirectionalReferenceIsPaired(courseInstance, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryProxyResolves(courseInstance, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_UniqueID(courseInstance, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryKeyUnique(courseInstance, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryMapEntryUnique(courseInstance, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateCourseInstance_validateEvaluation(courseInstance, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateCourseInstance_validateRoles(courseInstance, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateCourseInstance_validateHours(courseInstance, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the validateEvaluation constraint of '<em>Course Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String COURSE_INSTANCE__VALIDATE_EVALUATION__EEXPRESSION = "self.evaluations.percentage -> sum() = 100";

	/**
	 * Validates the validateEvaluation constraint of '<em>Course Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCourseInstance_validateEvaluation(CourseInstance courseInstance, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return validate(Tdt4250_assignmentPackage.Literals.COURSE_INSTANCE, courseInstance, diagnostics, context,
				"http://www.eclipse.org/emf/2002/Ecore/OCL", "validateEvaluation",
				COURSE_INSTANCE__VALIDATE_EVALUATION__EEXPRESSION, Diagnostic.ERROR, DIAGNOSTIC_SOURCE, 0);
	}

	/**
	 * The cached validation expression for the validateRoles constraint of '<em>Course Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String COURSE_INSTANCE__VALIDATE_ROLES__EEXPRESSION = "self.roles -> select(r | r.roleType = RoleType::COORDINATOR) -> size() >= 1";

	/**
	 * Validates the validateRoles constraint of '<em>Course Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCourseInstance_validateRoles(CourseInstance courseInstance, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return validate(Tdt4250_assignmentPackage.Literals.COURSE_INSTANCE, courseInstance, diagnostics, context,
				"http://www.eclipse.org/emf/2002/Ecore/OCL", "validateRoles",
				COURSE_INSTANCE__VALIDATE_ROLES__EEXPRESSION, Diagnostic.ERROR, DIAGNOSTIC_SOURCE, 0);
	}

	/**
	 * Validates the validateHours constraint of '<em>Course Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean validateCourseInstance_validateHours(
			CourseInstance instance,
			DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		
		// set required hours for schedule types
		int requiredLab = instance.getLabHours();
		int requiredLecture = instance.getLectureHours();

		Collection<StudyProgramCode> studyProgramCodes =
				instance.getStudyPrograms();
		
		// initialize map to counters
		Map<StudyProgramCode, Map<ScheduleSlotType, Integer>> programToCounterMap;
		programToCounterMap = new HashMap<>();
		for (StudyProgramCode program : studyProgramCodes) {
			programToCounterMap.put(program, new HashMap<>());
			programToCounterMap.get(program).put(ScheduleSlotType.LAB, 0);
			programToCounterMap.get(program).put(ScheduleSlotType.LECTURE, 0);
		}
		
		TimeTable timeTable = instance.getTimeTable();
		
		// add hours to counters
		for (ScheduleSlot slot : timeTable.getSchedule()) {
			int hours = slot.getTo() - slot.getFrom();
			for (StudyProgramCode studyProgram : slot.getStudyPrograms()) {
				if (studyProgramCodes.contains(studyProgram)) {
					ScheduleSlotType type = slot.getType();
					Map<ScheduleSlotType, Integer> counter = 
							programToCounterMap.get(studyProgram);
					int hoursForProgram = counter.get(type);
					counter.put(type, hoursForProgram + hours);
					programToCounterMap.put(studyProgram, counter);
				}
			}
		}
		
		// validate hours
		List<StudyProgramCode> invalidPrograms;
		invalidPrograms = new ArrayList<StudyProgramCode>();
		for (StudyProgramCode program : programToCounterMap.keySet()) {
			int actualLab = programToCounterMap
					.get(program).get(ScheduleSlotType.LECTURE);
			int actualLecture = programToCounterMap
					.get(program).get(ScheduleSlotType.EXERCISE);
			if (actualLab < requiredLab || actualLecture < requiredLecture) {
				invalidPrograms.add(program);
			}
		}

		if (!invalidPrograms.isEmpty()) {
			if (diagnostics != null) {
				for (StudyProgramCode studyProgram : invalidPrograms) {
					diagnostics.add(createDiagnostic(
						Diagnostic.ERROR, DIAGNOSTIC_SOURCE, 0,
						"Too few hours per week for " + studyProgram + ".",
						new Object[] {"validateHours", getObjectLabel(
								instance, context) }, new Object[] { instance },
						context));
				}
			}
			return false;
		}
		return true;

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateReduction(Reduction reduction, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(reduction, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRole(Role role, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(role, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDepartment(Department department, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(department, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePerson(Person person, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(person, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEvaluation(Evaluation evaluation, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(evaluation, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTimeTable(TimeTable timeTable, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(timeTable, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateScheduleSlot(ScheduleSlot scheduleSlot, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		if (!validate_NoCircularContainment(scheduleSlot, diagnostics, context))
			return false;
		boolean result = validate_EveryMultiplicityConforms(scheduleSlot, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryDataValueConforms(scheduleSlot, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryReferenceIsContained(scheduleSlot, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryBidirectionalReferenceIsPaired(scheduleSlot, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryProxyResolves(scheduleSlot, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_UniqueID(scheduleSlot, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryKeyUnique(scheduleSlot, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryMapEntryUnique(scheduleSlot, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateScheduleSlot_validateScheduleSlot(scheduleSlot, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the validateScheduleSlot constraint of '<em>Schedule Slot</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String SCHEDULE_SLOT__VALIDATE_SCHEDULE_SLOT__EEXPRESSION = "self.to > self.from";

	/**
	 * Validates the validateScheduleSlot constraint of '<em>Schedule Slot</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateScheduleSlot_validateScheduleSlot(ScheduleSlot scheduleSlot, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return validate(Tdt4250_assignmentPackage.Literals.SCHEDULE_SLOT, scheduleSlot, diagnostics, context,
				"http://www.eclipse.org/emf/2002/Ecore/OCL", "validateScheduleSlot",
				SCHEDULE_SLOT__VALIDATE_SCHEDULE_SLOT__EEXPRESSION, Diagnostic.ERROR, DIAGNOSTIC_SOURCE, 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateNamed(Named named, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(named, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSemesterType(SemesterType semesterType, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCourseCode(CourseCode courseCode, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateStudyProgramCode(StudyProgramCode studyProgramCode, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRoom(Room room, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateScheduleSlotType(ScheduleSlotType scheduleSlotType, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDay(Day day, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEvaluationType(EvaluationType evaluationType, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRoleType(RoleType roleType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		// TODO
		// Specialize this to return a resource locator for messages specific to this validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}

} //Tdt4250_assignmentValidator

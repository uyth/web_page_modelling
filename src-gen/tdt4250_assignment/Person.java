/**
 */
package tdt4250_assignment;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Person</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250_assignment.Person#getDepartment <em>Department</em>}</li>
 *   <li>{@link tdt4250_assignment.Person#getRoles <em>Roles</em>}</li>
 * </ul>
 *
 * @see tdt4250_assignment.Tdt4250_assignmentPackage#getPerson()
 * @model
 * @generated
 */
public interface Person extends Named {
	/**
	 * Returns the value of the '<em><b>Department</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link tdt4250_assignment.Department#getStaff <em>Staff</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Department</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Department</em>' container reference.
	 * @see #setDepartment(Department)
	 * @see tdt4250_assignment.Tdt4250_assignmentPackage#getPerson_Department()
	 * @see tdt4250_assignment.Department#getStaff
	 * @model opposite="staff" required="true" transient="false"
	 * @generated
	 */
	Department getDepartment();

	/**
	 * Sets the value of the '{@link tdt4250_assignment.Person#getDepartment <em>Department</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Department</em>' container reference.
	 * @see #getDepartment()
	 * @generated
	 */
	void setDepartment(Department value);

	/**
	 * Returns the value of the '<em><b>Roles</b></em>' reference list.
	 * The list contents are of type {@link tdt4250_assignment.Role}.
	 * It is bidirectional and its opposite is '{@link tdt4250_assignment.Role#getStaff <em>Staff</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Roles</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Roles</em>' reference list.
	 * @see tdt4250_assignment.Tdt4250_assignmentPackage#getPerson_Roles()
	 * @see tdt4250_assignment.Role#getStaff
	 * @model opposite="staff"
	 * @generated
	 */
	EList<Role> getRoles();

} // Person

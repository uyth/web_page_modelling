# Overview
This is a project that models the NTNU course pages with ECORE. The following wiki will describe the main classes, some of the constraints implemented and how to do a tranformation from the course model to web page model.

## Classes

### Course

A representation of a course. It is contained in a department. It has:
- CourseCode code

#### Reduction

A collection of related courses that gives credit reduction for the given course. It is contained within Course.

- CourseCode reductionCourse. What course will lead to reduction in credits
- double reduction. How many points are reduced for already done the reductionCourse.
- Date to. When the reduction ended.
- Date from. When the reduction started.

### CourseInstance

A representation of the course per semester. It is contained in Course. It has these attributes
- CourseCode. The course course eg. "TDT4100" 
- String content. A container for all the text given on the page.
- credits. The amount of credits given for taking the course
- StudyProgramCode[] studyPrograms. The study programs that has this course. 
- int hoursPerWeek. How many lecture, exercise and lab hours that is expected per week in total.
- int year
- SemesterType semester

Some other classes are contained within course.


#### TimeTable

A representation of the timetable in the web page. This class contains ScheduleSlot

##### ScheduleSlot

A representation of lectures, exercises and lab hours in the course. It is contained in TimeTable

- SchedyleSlotType type. What kind of lecture it is
- Day. What weekday it is
- int to. When it starts
- int from. When it ends
- room. What room it is in

#### Evaluation

A representation of the evaluation forms of the course. There can be more types. It is contained within Course.

- EvaluationType evalType. The type of evaluation
- int percentage. How many percents the evaluation form can give
- boolean required. Whether it is it required to do this evaluation form to take the exam

#### Role

A representation of a role for the course. This can be lecturer or coordinator for instance. A role must be referring to a person.


### Department

A representation of department. A course must be contained within a department. It contains staff

#### Person

A representation of a person. A person is contained within a department. A person can also have roles, eg. lecturer or coordinator 


## Constraints
Some of the constraints are done in the modeling, while others are explicitly written in AQL and Java. This sections will only mention the AQL and Java constraints.

- validateEvaluation (AQL)

  Checks if the evaluation forms of a course sums up to exactly 100 %

- validateRoles (AQL)

  Checks if there is at least one coordinator

- validateHours (Java)

  Checks if the number of hours for each study program corresponds to the amount in the course page. It can be higher than what is written in the course page because exercise hours are often split in several groups

- validateScheduleSlot (AQL)
  
  Checks if the from-time is before to-time for all the ScheduleSlots.

## Transformation

### Extensions

#### Xhtml1Factory
Adds create-methods so we can easily create new HTML-ETypes. Eg. ``createPType``

#### XhtmlUtil
Utility for the create-methods in Xhtml1Factory. Makes it so we can utilize the syntax:

```
createPType => [it += 'you text here']
```

### Performing the transformation

1. Clone the project
2. Locate tdt4250_assignment.xtend-gen.Course2WebPage
3. Right-click > Run As > [...]

   To run with the sample provided in the project, ...> Run As > Java Application

   To run with a source file and/or target file, ... > Run As > Run Configurations.., and go to the Arguments Pane. The first argument is the source file path, the second (optional) argument is the target file path. Apply and run.

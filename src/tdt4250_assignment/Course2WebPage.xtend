package tdt4250_assignment

import org.w3c.xhtml1.util.Xhtml1ResourceFactoryImpl
import org.w3c.xhtml1.Xhtml1Factory
import org.w3c.xhtml1.HtmlType
import java.io.ByteArrayOutputStream
import org.eclipse.emf.ecore.xmi.XMLResource
import tdt4250_assignment.XhtmlUtil
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.common.util.EList
import java.io.IOException
import java.util.Arrays
import java.io.PrintStream
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.eclipse.emf.ecore.EObject
import tdt4250_assignment.util.Tdt4250_assignmentResourceFactoryImpl
import org.w3c.xhtml1.TrType
import org.w3c.xhtml1.DivType
import org.w3c.xhtml1.UlType
import org.w3c.xhtml1.LiType
import org.w3c.xhtml1.PType
import org.w3c.xhtml1.BType
import java.util.ArrayList
import org.w3c.xhtml1.TableType
import tdt4250_assignment.HtmlTag

class Course2WebPage {

	Xhtml1ResourceFactoryImpl xhtml1ResourceFactoryImpl = 
		new Xhtml1ResourceFactoryImpl

	def String generateHtml(CourseInstance courseInstance) {
		val encoding = "UTF-8"
		val html = generateHtmlType(courseInstance);
		val fileName = (
			if (courseInstance.course.name !== null) {
				courseInstance.course.name.replace('.', '/')
			} else {
				"course"
			})+ '.html'
		val resource = xhtml1ResourceFactoryImpl
			.createResource(URI.createFileURI(fileName)) as XMLResource
		resource.getDefaultSaveOptions()
			.put(XMLResource.OPTION_ENCODING, encoding);
		resource.contents += html
		val outputStream = new ByteArrayOutputStream(4096)
		resource.save(outputStream, null)
		val originalOutput = outputStream.toString(encoding)
		originalOutput.cleanHtml
	}
	
	def cleanHtml(String html) {	
		html
			.replace("xhtml:", "")
			.replace("html_._type", "html")
			.replace("xmlns:xhtml=", "xmlns=")
			.replace("&lt;", "<")
			.replace("&gt;", ">")
			.replace("&amp;", "&")
			.replace("&quot;", "'")
	}
	
	extension Xhtml1Factory xhtml1Factory = Xhtml1Factory.eINSTANCE
			
	extension XhtmlUtil xhtmlUtil = new XhtmlUtil

			
	def HtmlType generateHtmlType(CourseInstance courseInstance) {
		val html = createHtmlType => [
			head = createHeadType => [
				title = createTitleType => [
					it += courseInstance.course.name
				]
				meta += createMetaType => [
					httpEquiv = "content-type"
					content = "text/html; charset=UTF-8"
				]
			]
			body = createBodyType => [
				it += generate(courseInstance.course.code + 
                    " - " + courseInstance.course.name, HtmlTag.H1)
				it += generateAbout(courseInstance)
				it += generateTimeTable(courseInstance.timeTable)
				it += generateFacts(courseInstance)
				it += generateCourseWork(courseInstance)
				it += generateContactInformation(courseInstance)
			]
		]
		html
	}
	
	def DivType generateAbout(CourseInstance instance) {
		createDivType => [			
			it += generate("About", HtmlTag.H2)
			it += generateEvaluations(instance.evaluations)
			it += generateContent(instance.content)
			it += generateReductions(instance.course.creditReductions)
		]
	}
		
	def DivType generateEvaluations(EList<Evaluation> evaluations) {
		createDivType => [
			if (!evaluations.empty) {
			    val rows = new ArrayList<TrType>()
                rows += generateTrHeader('Evaluation Form', 'Weighting')
				for (evaluationEntry : evaluations) {
					rows += generateEvaluationEntry(evaluationEntry)
				}
				it += generateTable(rows)
			} else {
				it += generate("No evaluations")
			}
		]
	}
	
	def TrType generateEvaluationEntry(Evaluation evaluation) {
		generateTr(evaluation.evalType.getName, evaluation.percentage.toString)
	}
	
	def DivType generateContent(String content) {
		createDivType => [
			it += generate("Course Content", HtmlTag.H3)
			it += generate(content)
		]	
	}
		
	def DivType generateReductions(EList<Reduction> reductions) {
		createDivType => [
			it += generate("Reductions", HtmlTag.H3)
			if (!reductions.empty) {
    			val rows = new ArrayList<TrType>()
    			rows += generateTrHeader('Course Code', 'Reduction', 'From',
    			                         'To')
                for (reduction : reductions) {
                    rows += generateReductionEntry(reduction)
                }
				it += generateTable(rows)
			} else {
				it += generate("No reductions")
			}
		]
	}
		
	def TrType generateReductionEntry(Reduction reduction) {
	    val list = Arrays.asList(
			reduction.reductionCourse.toString,
			reduction.reduction.toString,
			(reduction.from ?: "").toString,
			(reduction.to ?: "").toString
	    )
		generateTr(list)
	}
	
	def DivType generateTimeTable(TimeTable table) {
		createDivType => [
			it += generate("Time table", HtmlTag.H2)
			if (!table.schedule.empty) {
			    val rows = new ArrayList<TrType>()
			    rows += generateTrHeader("Day", "From", "To", "Study Program")
				for (schedule : table.schedule) {
					rows += generateSchedule(schedule)
				}
				it += generateTable(rows)
			} else {
				it += generate("No schedule found")
			}
		]
	}
	
	def TrType generateSchedule(ScheduleSlot slot) {
	    val list = Arrays.asList(
	        slot.day.toString,
	        slot.from.toString,
	        slot.to.toString,
	        // TOOD: TEST studyprograms
	        slot.studyPrograms.join
	    )
		generateTr(list)
	}

	def DivType generateFacts(CourseInstance instance) {
		createDivType => [
			it += generate("Facts", HtmlTag.H2)
			it += generateUl("Credits: " + instance.credits)
		]
	}
	
	def DivType generateCourseWork(CourseInstance instance) {
		createDivType => [
			it += generate("Course Work", HtmlTag.H2)
            val listString = new ArrayList<String>()
            listString += "Teching semester: " + instance
                .semester.toString + " " + instance.year
            if (instance.lectureHours > 0) {
                listString += "Number of hours per week: "
                    + instance.lectureHours
            }
            if (instance.labHours > 0) {
                listString += "Number of hours per week: "
                    + instance.labHours
            }
			it += generateUl(listString)
		]
	}
			
	def DivType generateContactInformation(CourseInstance instance) {
		createDivType => [
			it += generate("Contact information", HtmlTag.H2)
			it += generate(
				RoleType.COORDINATOR.toString.toLowerCase.toFirstUpper,
				HtmlTag.B
			)
			it += generateStaffList(instance.roles, RoleType.COORDINATOR)
			it += generate(
				RoleType.LECTURER.toString.toLowerCase.toFirstUpper,
				HtmlTag.B
			)
			it += generateStaffList(instance.roles, RoleType.LECTURER)
			it += generate("Responsible department", HtmlTag.B)
			it += generate(instance.course.department.name)
		]
	}
	
	def UlType generateStaffList(EList<Role> roles, RoleType type) {
	    val staffList = new ArrayList()
		for (role : roles) {
			if (role.roleType == type) {
				staffList += role.staff.name
			}
		}
		generateUl(staffList)
	}
	
	def TableType generateTable(TrType... rows) {
	   createTableType => [
	       for (row : rows) tr += row
	   ]   
	}
	
	def UlType generateUl(String... list) {
	    createUlType => [
	        for (listElement : list) {
	            li += generate(listElement, HtmlTag.LI) as LiType
	        }
	    ]
	}
	   
    def generateTrHeader(String... headers) {
        createTrType => [
            for (head : headers) {
                th += createThType => [it += head]
            }
        ]
    }

	def TrType generateTr(String... data) {
		createTrType => [
			for (dataEntry : data) {
				if (data !== null) td += createTdType => [it += dataEntry]
			}
		]
	}

    def EObject generate(String text, HtmlTag type) {
    	switch type {
            case HtmlTag.P: return generatePType(text)
            case HtmlTag.B: return generateBType(text)
            case HtmlTag.H1: return generateH1Type(text)
            case HtmlTag.H2: return generateH2Type(text)
            case HtmlTag.H3: return generateH3Type(text)
            case HtmlTag.LI: return generateLiType(text)
            default: return generatePType(text)
    	}	  	
    }
    
    def PType generate(String text) {
        return generatePType(text)
    }
    
    def generateH1Type(String text) {
    	createH1Type => [it += text]
    }

    def generateH2Type(String text) {
    	createH2Type => [it += text]
    }
    
    def generateH3Type(String text) {
    	createH3Type => [it += text]
    }

    def PType generatePType(String text) {
    	createPType => [it += text]
    }

    def BType generateBType(String text) {
    	createBType => [it += text]
    }

    def LiType generateLiType(String text) {
        createLiType => [it += text]
    }
    
	def static void main(String[] args) throws IOException {
		val argsAsList = Arrays.asList(args)
		val department = if (argsAsList.size > 0) {
		    // get file at uri
			tdt4250_assignment.Course2WebPage.getDepartment(argsAsList.get(0))
		} else {
		    // no args -> get sample
			tdt4250_assignment.Course2WebPage.getSampleDepartment()
		};
		for (course : department.courses) {
			for (courseInstance : course.courseInstances) {				
				val html = new Course2WebPage().generateHtml(courseInstance);
				// has target output uri
				if (args.length > 1) {
					val target = URI.createURI(argsAsList.get(1));
					val ps = new PrintStream(
						department.eResource().getResourceSet()
						.getURIConverter().createOutputStream(target)
					)
					ps.print(html);
				} else {
				    // standard output
					System.out.println(html);
				}
			}			
		}
	}
	
	def static Department getDepartment(String uriString) throws IOException {
		val resSet = new ResourceSetImpl();
		resSet.getPackageRegistry().put(Tdt4250_assignmentPackage.eNS_URI,
										Tdt4250_assignmentPackage.eINSTANCE
		);
		resSet.getResourceFactoryRegistry().getExtensionToFactoryMap()
			.put("xmi", new Tdt4250_assignmentResourceFactoryImpl());
		val resource = resSet.getResource(URI.createURI(uriString), true);
		for (EObject eObject : resource.getContents()) {
			if (eObject instanceof Department) {
				return eObject as Department;
			}
		}
		throw new IOException("Could not fetch file with the uri " + uriString)
	}
	
	def static Department getSampleDepartment() {
		try {
			return tdt4250_assignment.Course2WebPage.getDepartment(
				Course2WebPage.getResource("Department.xmi").toString()
			);
		} catch (IOException e) {
			System.err.println(e);
			return null;
		}
	}	
	
}
